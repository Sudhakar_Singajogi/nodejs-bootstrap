import React, { Fragment } from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import ContactUs from "../LandingPage/Sections/ContactUs";
import HeaderLinks from "components/Header/HeaderLinks.js";
import Header from "components/Header/Header.js";
import Footer from "components/Footer/Footer.js";
import { Zoom, Reveal, Fade } from "react-reveal";

import classNames from "classnames";
import Parallax from "components/Parallax/Parallax.js";
import GridContainer from "components/Grid/GridContainer.js";

import ProjectSection from "views/LandingPage/Sections/ProjectSection";
import ServiceSection from "views/LandingPage/Sections/ServiceSection";
import GridItem from "components/Grid/GridItem.js";
import image from "assets/img/iwhylogo.jpg";
import CONFIG from "utils/config";
import styles from "./styles.js";
const dashboardRoutes = [];
const useStyles = makeStyles(styles);

const Home = (props) => {
  const classes = useStyles();
  const { ...rest } = props;
  return (
    <Fragment>
      <Header
        routes={dashboardRoutes}
        brand={<img src={image} width="80" alt="iwhyLogo" />}
        rightLinks={<HeaderLinks data={CONFIG.home} />}
        color="white"
        fixed
        changeColorOnScroll={{
          height: 400,
          color: "white"
        }}
        {...rest}
      />
      <Parallax filter image={require("assets/img/landing-bg.jpg")}>
        <div className={classes.container}>
          <GridContainer>
            <GridItem xs={12} sm={12} md={6}>
              <Reveal right cascade>
                <h1 className={classes.title}>Need Solutions for Products?</h1>
                <h4>
                  We will help you to drive your Product from every aspect from designs solution,
                  technical architecture, Deployment and Management solutions. Why wait and think?
                  Leave your details will reach you out.
                </h4>
              </Reveal>
            </GridItem>
          </GridContainer>
        </div>
      </Parallax>
      <div className={classNames(classes.main, classes.mainRaised)}>
        <div className={classes.container}>
          <Zoom>
            <ServiceSection />
          </Zoom>
          <Zoom>
            <ProjectSection />
          </Zoom>
        </div>
      </div>
      <Fade left>
        <ContactUs />
      </Fade>
      <Footer data={CONFIG.footerNote} />
    </Fragment>
  );
};

export default Home;
