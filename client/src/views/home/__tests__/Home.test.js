import React from "react";
import { render, screen, cleanup } from "@testing-library/react";
//https://www.freecodecamp.org/news/8-simple-steps-to-start-testing-react-apps-using-react-testing-library-and-jest/
//https://www.robinwieruch.de/react-testing-library

import Home from "../Home";

afterEach(cleanup);

describe("Home", () => {
  it("renders Home component", () => {
    const { asFragment } = render(<Home />);

    expect(asFragment(<Home />)).toMatchSnapshot();
  });

  it("Nav should have Contact link", () => {
    const { getByTestId } = render(<Home />);
    expect(getByTestId("test-contact")).toHaveTextContent("Contact");
  });
});
