import React from "react";
import { render, screen, cleanup } from "@testing-library/react";
//https://www.freecodecamp.org/news/8-simple-steps-to-start-testing-react-apps-using-react-testing-library-and-jest/
//https://www.robinwieruch.de/react-testing-library

import ProfilePage from "../ProfilePage";

afterEach(cleanup);

describe("ProfilePage", () => {
  it("renders ProfilePage component", () => {
    const { asFragment } = render(<ProfilePage />);

    expect(asFragment(<ProfilePage />)).toMatchSnapshot();
  });
});
