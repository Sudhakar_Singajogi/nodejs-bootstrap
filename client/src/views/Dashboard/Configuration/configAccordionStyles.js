const configAccordionStyles = (theme) => ({
  root: {
    width: "100%",
    background: "#1B2430",

    "& svg": {
      color: "#EEEEEE"
    },
    "& div.MuiExpansionPanelSummary-root.Mui-expanded": {
      minHeight: 0
    },
    "& div.MuiExpansionPanelSummary-content.Mui-expanded": {
      margin: 0
    },
    "& .MuiExpansionPanelDetails-root": {
      paddingTop: 0,
      paddingBottom: 0,
      background: "#121820",
      "& a": {
        "&:hover": {
          color: "#FF5533"
        }
      }
    }
  },

  expansionPanel: {
    background: "#1B2430",
    color: "white",
    "&:hover": {},
    "& a": {
      color: "white",
      textDecoration: "none"
    }
  },
  ExpansionPanelSummaryExpandedIcon: {
    paddingLeft: 0,
    "& div.MuiExpansionPanelSummary-expandIcon": {
      order: "-1"
    }
  },
  expansionDetails: {
    paddingLeft: "50px",
    position: "relative",
    "& p": {
      color: "#A7AAAE",
      "&:hover": {
        color: "#ffffff",
        cursor: "pointer"
      }
    }
  },

  heading: {
    fontSize: theme.typography.pxToRem(15),
    flexShrink: 0,
    paddingLeft: "10px"
  },
  subHeading: {
    display: "block",
    margin: "10px 0"
  },
  secondaryHeading: {
    fontSize: theme.typography.pxToRem(15),
    color: theme.palette.text.secondary,
    paddingLeft: "10px"
  }
});

export default configAccordionStyles;
