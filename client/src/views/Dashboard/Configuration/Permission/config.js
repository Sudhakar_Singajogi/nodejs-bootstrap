export default {
  permissions: {
    features: [
      {
        id: "Messages",
        label: "Messages"
      },
      {
        id: "Permissions",
        label: "Permissions"
      },
      {
        id: "Roles",
        label: "Roles"
      },
      {
        id: "Users",
        label: "Users"
      },
      {
        id: "VTChannel",
        label: "VTChannel"
      },
      {
        id: "VTC",
        label: "VTC"
      },
      {
        id: "Vendors",
        label: "Vendors"
      }
    ]
  }
};
