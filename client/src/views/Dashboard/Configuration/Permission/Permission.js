import React, { useEffect, useState } from "react";
import { ThemeProvider, makeStyles, createMuiTheme, useTheme } from "@material-ui/core/styles";
import { useDispatch, useSelector } from "react-redux";
import { listPermissions, togglePermissionStatus } from "./_duck/action";
import { purple } from "@material-ui/core/colors";
import Avatar from "@material-ui/core/Avatar";
import Switch from "@material-ui/core/Switch";
import Alert from "@material-ui/lab/Alert";
import Grid from "@material-ui/core/Grid";
import styles from "./PermissionStyles.js";
import Button from "@material-ui/core/Button";
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import CONFIG from "./config";
import Checkbox from "@material-ui/core/Checkbox";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";
import IconButton from "@material-ui/core/IconButton";
import Badge from "@material-ui/core/Badge";
import Container from "@material-ui/core/Container";
// import Grid from '@material-ui/core/Grid';
import Paper from "@material-ui/core/Paper";
import Box from "@material-ui/core/Box";
import { listRoles } from "./_duck/action";
import EditIcon from "@material-ui/icons/Edit";
import Loader from "../../../../components/Loader/Loader";
import SnackbarContent from "../../../../components/Snackbar/SnackbarContent.js";
import Clearfix from "../../../../components/Clearfix/Clearfix.js";
import { Link } from "react-router-dom";
import EditOutlinedIcon from "@material-ui/icons/EditOutlined";
import PermissionDialog from "./PermissionDialog";
import FirstPageIcon from "@material-ui/icons/FirstPage";
import KeyboardArrowLeft from "@material-ui/icons/KeyboardArrowLeft";
import KeyboardArrowRight from "@material-ui/icons/KeyboardArrowRight";
import LastPageIcon from "@material-ui/icons/LastPage";
import TableFooter from "@material-ui/core/TableFooter";
import TablePagination from "@material-ui/core/TablePagination";
import TablePaginationActions from "../../../../components/TablePagination/TablePaginationActions";
let permissionsTemp = CONFIG.permissions;

const theme = createMuiTheme({
  palette: {
    primary: purple
  }
});
const useStyles = makeStyles(styles);

export default function Permission() {
  const dispatch = useDispatch();
  const permissionList = useSelector((state) => state.permissionList);
  const { permissions, error: errorList } = permissionList;
  // console.log(`permission list : ${JSON.stringify(permissions)}`);
  const permissionCreate = useSelector((state) => state.permissionCreate);
  let { loading, error } = permissionCreate;
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);
  const emptyRows = rowsPerPage - Math.min(rowsPerPage, permissions.length - page * rowsPerPage);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const auth = useSelector((state) => state.auth);
  const { userInfo } = auth;

  const classes = useStyles();
  // const [permissionsData, setPermissionsData] = useState(null);
  // const [message, setMessage] = useState(null);

  const loadData = async () => {
    try {
      dispatch(listPermissions());

      // const permissions = await axiosGetData("/api/permissions");
      // if (permissions) setPermissionsData(permissions.data);
    } catch (err) {
      // console.log(err);
      // setMessage("Error in displaying data");
    }
  };

  const handleStatusChange = async (event, role) => {
    // const roleId = role.id;
    // let body = {
    //   status: event.target.checked ? "Active" : "InActive"
    // };
    // console.log(`request body: ${JSON.stringify(body)}`);
    // const loadRoles = await axiosPatchData("/api/roles/" + roleId, body);
    // loadRoles && loadData();
  };

  useEffect(() => {
    loadData();
    return () => {};
  }, []);

  const handleChange = async (event, item) => {
    // console.log("toggle clicked");
    // console.log(`logging item: ${JSON.stringify(item)}`);
    // console.log("user object "+ JSON.parse(sessionStorage.getItem("userinfo").Role));

    let body = {
      name: item.name,
      permissionList: item.permissionList,
      status: item.status == "Active" ? "InActive" : "Active"
    };
    dispatch(togglePermissionStatus(item.id, body));
    // update the status to back-end
    // togglePermissionStatus(item);
    // update the state
  };

  const updateData = () => loadData();

  return (
    <ThemeProvider theme={theme}>
      <div className={classes.permissionContainer}>
        <div className={classes.headingWrapper}>
          <h1>Permissions List</h1>
          <Link to="/dashboard/config/AddPermission">
            <Button color="primary" className={classes.buttonStyles}>
              Add Permission
            </Button>
          </Link>
        </div>

        <GridContainer
          container
          style={{ marginTop: "35px" }}
          justify="flex-start"
          direction="row"
          alignItems="center"
          spacing={4}
          className={classes.permissionWrapper}
        >
          {userInfo.PermisionList &&
            Object.entries(userInfo.PermisionList[0]).map(([key, value]) => (
              <GridItem key={key} className={classes.permissionList}>
                <div>{key} : </div>
                <div>{value}</div>
              </GridItem>
            ))}
        </GridContainer>

        <Container
          maxWidth="md"
          className={classes.container}
          style={{ marginTop: "35px", marginBottom: "55px" }}
        >
          <Grid container spacing={3}>
            <Grid item xs={12}>
              <Paper className={classes.paper}>
                <div>
                  <Table>
                    <TableHead>
                      <TableRow>
                        {/* <TableCell>Permission Id</TableCell> */}
                        <TableCell>Permission Name</TableCell>
                        <TableCell>Feature Access Controls</TableCell>
                        <TableCell>Status</TableCell>
                        <TableCell align="right">Edit Permission</TableCell>
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      {permissions &&
                        (rowsPerPage > 0
                          ? permissions.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                          : permissions
                        ).map((permission) => (
                          <TableRow key={permission.id}>
                            {/* <TableCell>{permission.id}</TableCell> */}
                            <TableCell>{permission.name}</TableCell>
                            <TableCell>
                              {/* <div className={classes.paperWrap}> */}
                              <PermissionDialog item={permission.permissionList[0]} />
                              {/* </div> */}
                            </TableCell>
                            <TableCell>
                              {
                                <Checkbox
                                  checked={permission.status === "Active" ? true : false}
                                  onChange={(event) => handleStatusChange(event, permission)}
                                  color="primary"
                                  inputProps={{ "aria-label": "secondary checkbox" }}
                                />
                              }
                            </TableCell>
                            <TableCell align="right">
                              <Link
                                color="primary"
                                to={`/dashboard/config/EditPermissions/${permission.id}`}
                              >
                                <EditOutlinedIcon />
                              </Link>
                            </TableCell>
                          </TableRow>
                        ))}
                    </TableBody>
                    <TableFooter>
                      <TableRow>
                        <TablePagination
                          rowsPerPageOptions={[5, 10, 25, { label: "All", value: -1 }]}
                          colSpan={3}
                          count={permissions.length}
                          rowsPerPage={rowsPerPage}
                          page={page}
                          SelectProps={{
                            inputProps: { "aria-label": "rows per page" },
                            native: true
                          }}
                          onChangePage={handleChangePage}
                          onChangeRowsPerPage={handleChangeRowsPerPage}
                          ActionsComponent={TablePaginationActions}
                        />
                      </TableRow>
                    </TableFooter>
                  </Table>
                </div>
              </Paper>
            </Grid>
          </Grid>
        </Container>
      </div>
    </ThemeProvider>
  );
}
