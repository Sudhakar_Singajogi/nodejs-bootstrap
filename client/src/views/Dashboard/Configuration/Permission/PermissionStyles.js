import { purple } from "@material-ui/core/colors";
const PermissionStyles = (theme) => ({
  vendorsWrap: {
    display: "flex",
    flexWrap: "wrap"
  },
  margin40: {
    marginLeft: "40px"
  },
  margin30: {
    marginLeft: "30px"
  },
  permissionWrapper: {
    width: "100%",
    margin: 0,
    marginBottom: "20px",
    boxShadow: "0px 1px 5px #CCC"
  },

  addpermission: {
    boxShadow: "0px 1px 5px #CCC",
    padding: "26px",
    marginTop: "115px",
    "& legend": {
      color: "#8e24aa"
    }
  },
  permissionList: {
    display: "flex",
    flexWrap: "wrap",
    width: "25%",
    "& div": {
      width: "44%",
      "&:nth-child(2)": {
        color: "#8e24aa"
      }
    },
    "& div:firstChild": {
      color: "#8e24aa"
    }
  },
  permissionWrap: {
    display: "flex",
    flexWrap: "wrap",
    width: "100%"
  },
  accordionWrapper: {
    padding: 0
  },
  root: {
    width: "100%",
    margin: 0,
    marginBottom: "20px",
    minHeight: 250,
    boxShadow: "0px 1px 5px #CCC",
    transition: "all .5s",
    [theme.breakpoints.up("sm")]: {
      width: "50%"
    },
    [theme.breakpoints.up("lg")]: {
      width: "25%"
    },
    "& strong": {
      color: "#8e24aa"
    },
    "&:hover": {
      transform: "scale(1.10)",
      background: "#fafafa"
    }
  },
  largeAvatar: {
    width: theme.spacing(8),
    height: theme.spacing(8),
    color: "#fff",
    backgroundColor: purple[700]
  },
  headingWrapper: {
    width: "100%",
    display: "flex",
    "& h1": {
      flexGrow: "1"
    }
  },
  permissionContainer: {
    display: "flex",
    flexWrap: "wrap",
    "& a": {
      textDecoration: "none"
    }
  },
  buttonStyles: {
    marginTop: "24px",
    background: "#8e24aa",
    color: "#fff",
    "&:hover": {
      backgroundColor: "#8e24aa",
      color: "#fff"
    }
  },
  lastSection: {
    borderTop: "1px solid #EEE",
    width: "100%",
    alignItems: "center",
    marginTop: "10px"
  },
  paperWrap: {
    padding: theme.spacing(0.5),
    borderRight: "1px solid #EEE",
    fontSize: "13px",
    textAlign: "center",
    color: theme.palette.text.secondary
  },
  gridSpacing: {
    padding: "5px",
    width: "25%",
    "& > div": {
      flexDirection: "row"
    },
    "& label": {
      width: "95px"
    }
  }
});
export default PermissionStyles;
