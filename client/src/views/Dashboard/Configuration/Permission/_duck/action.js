import {
  PERMISSION_LIST_FAIL,
  PERMISSION_LIST_REQUEST,
  PERMISSION_LIST_SUCCESS,
  PERMISSION_UPDATE_FAIL,
  PERMISSION_UPDATE_REQUEST,
  PERMISSION_UPDATE_SUCCESS,
  TOGGLE_PERMISSION_STATUS,
  PERMISSION_CREATE_FAIL,
  PERMISSION_CREATE_REQUEST,
  PERMISSION_CREATE_SUCCESS,
  TOGGLE_STATUS_FAIL,
  TOGGLE_STATUS_REQUEST,
  TOGGLE_STATUS_SUCCESS,
  PERMISSION_DETAILS_FAIL,
  PERMISSION_DETAILS_REQUEST,
  PERMISSION_DETAILS_SUCCESS
} from "./type";
import { axiosGetData, axiosPatchData, axiosPostData } from "utils/util";

export const createPermission = (body, history) => async (dispatch) => {
  // console.log(`print create permissoin body from action: ${JSON.stringify(body)}`);
  try {
    dispatch({
      type: PERMISSION_CREATE_REQUEST
    });

    const { data } = await axiosPostData(`/api/permissions`, body);

    dispatch({
      type: PERMISSION_CREATE_SUCCESS,
      payload: data
    });

    history.push("/dashboard/config/Permission");
  } catch (error) {
    dispatch({
      type: PERMISSION_CREATE_FAIL,
      payload:
        error.response && error.response.data.message ? error.response.data.message : error.message
    });
  }
};

export const togglePermissionStatus = (id, body, history = null) => async (dispatch) => {
  try {
    dispatch({
      type: TOGGLE_STATUS_REQUEST
    });
    const permission = await axiosPatchData(`/api/permissions/${id}`, body);

    dispatch({
      type: TOGGLE_STATUS_SUCCESS,
      payload: permission.data
    });

    // update client side
    dispatch({
      type: TOGGLE_PERMISSION_STATUS,
      payload: permission.data
    });

    if (history) {
      history.push("/dashboard/config/Permission");
    }
  } catch (error) {
    dispatch({
      type: TOGGLE_STATUS_FAIL,
      payload:
        error.response && error.response.data.message ? error.response.data.message : error.message
    });
  }
};

export const listPermissions = () => async (dispatch) => {
  try {
    dispatch({
      type: PERMISSION_LIST_REQUEST
    });
    const permissions = await axiosGetData("/api/permissions");

    // console.log(`response data : ${JSON.stringify(permissions)}`);
    dispatch({
      type: PERMISSION_LIST_SUCCESS,
      payload: permissions.data
    });
  } catch (error) {
    dispatch({
      type: PERMISSION_LIST_FAIL,
      payload:
        error.response && error.response.data.message ? error.response.data.message : error.message
    });
  }
};

export const getPermissionDetails = (id) => async (dispatch) => {
  try {
    dispatch({
      type: PERMISSION_DETAILS_REQUEST
    });

    const { data } = await axiosGetData(`/api/permissions/${id}`);

    dispatch({
      type: PERMISSION_DETAILS_SUCCESS,
      payload: data
    });
  } catch (error) {
    dispatch({
      type: PERMISSION_DETAILS_FAIL,
      payload:
        error.response && error.response.data.message ? error.response.data.message : error.message
    });
  }
};
