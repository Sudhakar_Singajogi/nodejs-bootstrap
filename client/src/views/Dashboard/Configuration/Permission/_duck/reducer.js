import {
  PERMISSION_LIST_REQUEST,
  PERMISSION_LIST_SUCCESS,
  PERMISSION_LIST_FAIL,
  TOGGLE_PERMISSION_STATUS,
  PERMISSION_CREATE_FAIL,
  PERMISSION_CREATE_REQUEST,
  PERMISSION_CREATE_SUCCESS,
  TOGGLE_STATUS_FAIL,
  TOGGLE_STATUS_REQUEST,
  TOGGLE_STATUS_SUCCESS,
  PERMISSION_DETAILS_FAIL,
  PERMISSION_DETAILS_REQUEST,
  PERMISSION_DETAILS_SUCCESS
} from "./type";

const INITIAL_STATE = {
  permissions: []
};

export const permissionCreateReducer = (state = {}, action) => {
  switch (action.type) {
    case PERMISSION_CREATE_REQUEST:
    case TOGGLE_STATUS_REQUEST:
      return { loading: true };
    case PERMISSION_CREATE_SUCCESS:
    case TOGGLE_STATUS_SUCCESS:
      return { loading: false, error: "", permission: action.payload };
    case PERMISSION_CREATE_FAIL:
    case TOGGLE_STATUS_FAIL:
      return { loading: false, error: action.payload };
    default:
      return state;
  }
};

// export const togglePermissionStatusReducer = (state = { permission: {} }, action) => {
//   switch (action.type) {
//     case PERMISSION_UPDATE_REQUEST:
//       return { loading: true };
//     case PERMISSION_UPDATE_SUCCESS:
//       return { loading: false, permission: action.payload };
//     case PERMISSION_UPDATE_FAIL:
//       return { loading: false, error: action.payload };
//     default:
//       return state;
//   }
// };

export const permissiontListReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case PERMISSION_LIST_REQUEST:
      return { loading: true, permissions: [] };
    case PERMISSION_LIST_SUCCESS:
      return {
        loading: false,
        permissions: action.payload
      };
    case PERMISSION_LIST_FAIL:
      return { loading: false, error: action.payload };
    case TOGGLE_PERMISSION_STATUS:
      return { permissions: togglePermissionStatus(state, action.payload) };
    default:
      return state;
  }
};

export const permissionDetailsReducer = (state = { permission: {} }, action) => {
  switch (action.type) {
    case PERMISSION_DETAILS_REQUEST:
      return { ...state, loading: true };
    case PERMISSION_DETAILS_SUCCESS:
      return {
        loading: false,
        permission: action.payload
      };
    case PERMISSION_DETAILS_FAIL:
      return { loading: false, error: action.payload };
    default:
      return state;
  }
};

// toggle permission status
const togglePermissionStatus = (state, payload) => {
  let newPermissions = [...state.permissions];
  let permissionIndex = newPermissions.findIndex((item) => item.id == payload.id);
  newPermissions[permissionIndex] = { ...newPermissions[permissionIndex], status: payload.status };
  return newPermissions;
};
