import * as Actions from "./action";
import { axiosLoginData } from "utils/util";
/*
{
        email: "arun.reddy143@gmail.com",
        password: "test123"
      }*/

const loadUserInfo = (data, history) => {
  return async (dispatch) => {
    try {
      let axiosResp = await axiosLoginData(data);
      if (axiosResp.error) {
        // console.log(axiosResp, "axios check");
        return;
      }
      sessionStorage.setItem("refreshToken", axiosResp.refreshToken);
      sessionStorage.setItem("accessToken", axiosResp.accessToken);
      sessionStorage.setItem("user", axiosResp.user.email);
      sessionStorage.setItem("userRole", axiosResp.user.Role);
      sessionStorage.setItem("messages", axiosResp.user.PermisionList[0].Messages);
      sessionStorage.setItem("permissions", axiosResp.user.PermisionList[0].Permissions);
      sessionStorage.setItem("roles", axiosResp.user.PermisionList[0].Roles);
      sessionStorage.setItem("users", axiosResp.user.PermisionList[0].Users);
      sessionStorage.setItem("vtc", axiosResp.user.PermisionList[0].VTC);
      sessionStorage.setItem("vtchannel", axiosResp.user.PermisionList[0].VTChannel);
      sessionStorage.setItem("vendors", axiosResp.user.PermisionList[0].Vendors);

      dispatch(Actions.getUserInfo(axiosResp.user));

      history.push(`/dashboard`);
    } catch (err) {
      dispatch(Actions.toggleErrorInfo(err.error));
    }
  };
};

export { loadUserInfo };
