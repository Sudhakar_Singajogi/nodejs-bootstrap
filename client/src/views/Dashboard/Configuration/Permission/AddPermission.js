import React, { useState, useEffect } from "react";
import styles from "./PermissionStyles.js";
import { makeStyles } from "@material-ui/core/styles";
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import FormControl from "@material-ui/core/FormControl";
import FormLabel from "@material-ui/core/FormLabel";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import RadioGroup from "@material-ui/core/RadioGroup";
import Radio from "@material-ui/core/Radio";
import Button from "@material-ui/core/Button";
import { Link } from "react-router-dom";
import CONFIG from "./config";
import CustomInput from "components/CustomInput/CustomInput.js";
import TextField from "@material-ui/core/TextField";
import { useDispatch, useSelector } from "react-redux";
import { createPermission } from "./_duck/action";

const useStyles = makeStyles(styles);
export default function AddPermission({ history }) {
  // console.log(JSON.stringify(props));
  const classes = useStyles();
  const [value, setValue] = React.useState("read");
  const dispatch = useDispatch();
  const [name, setName] = useState("");
  const [featureAccess, setfeatureAccess] = useState({
    Messages: "",
    Permissions: "",
    Roles: "",
    Users: "",
    VTChannel: "",
    VTC: "",
    Vendors: ""
  });

  const [permissionName, setPermissionName] = useState("");

  const handlePermissionChange = (event) => {
    setPermissionName(event.target.value);
  };

  const handleChange = (event, key) => {
    let value = event.target.value;
    setfeatureAccess((prevState) => {
      let tempState = { ...prevState };
      tempState[key] = value;
      return tempState;
    });
  };

  const submitHandler = (e) => {
    e.preventDefault();
    let permissionList = { ...featureAccess };

    Object.entries(permissionList).map(
      ([key, value]) => (permissionList[key] = value.toUpperCase())
    );

    let body = {
      name: permissionName,
      permissionList: [permissionList]
    };

    dispatch(createPermission(body, history));
  };

  return (
    <div className={classes.permissionContainer}>
      <div className={classes.headingWrapper}>
        <h1>Add Permissions</h1>
        <Link to="/dashboard/config/Permission">
          <Button className={classes.buttonStyles} color="primary">
            Permission List
          </Button>
        </Link>
        <Link style={{ textDecoration: "none" }}>
          <Button
            color="primary"
            onClick={submitHandler}
            className={[classes.buttonStyles, classes.margin30]}
          >
            Add Permission
          </Button>
        </Link>
      </div>
      <div>
        <TextField
          id="permission-name"
          label="Enter Permission Name"
          value={permissionName}
          onChange={handlePermissionChange}
        />
      </div>
      <FormControl component="fieldset" className={classes.addpermission}>
        <GridContainer
          container
          justify="flex-start"
          direction="row"
          spacing={4}
          className={classes.root1}
        >
          {Object.entries(featureAccess).map(([key, value]) => (
            <GridItem key={key} className={classes.gridSpacing}>
              <FormLabel component="legend" style={{ marginBottom: "15px" }}>
                {key}
              </FormLabel>
              <select value={value} onChange={(e) => handleChange(e, key)}>
                <option value="">Select</option>
                <option value="R">R</option>
                <option value="RW">RW</option>
                <option value="RWX">RWX</option>
                <option value="NONE">None</option>
              </select>
            </GridItem>
          ))}
        </GridContainer>
      </FormControl>
    </div>
  );
}
