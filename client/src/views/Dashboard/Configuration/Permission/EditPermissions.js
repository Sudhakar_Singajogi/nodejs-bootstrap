import React, { useState, useEffect } from "react";
import styles from "./PermissionStyles.js";
import { makeStyles } from "@material-ui/core/styles";
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import FormControl from "@material-ui/core/FormControl";
import FormLabel from "@material-ui/core/FormLabel";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import RadioGroup from "@material-ui/core/RadioGroup";
import Radio from "@material-ui/core/Radio";
import Button from "@material-ui/core/Button";
import { Link } from "react-router-dom";
import CONFIG from "./config";
import CustomInput from "components/CustomInput/CustomInput.js";
import AllSchemas from "utils/validationSchema";
import { useFormik } from "formik";
import { axiosPostData, axiosPatchData } from "utils/util";
import { useDispatch, useSelector } from "react-redux";
import { getPermissionDetails, togglePermissionStatus } from "./_duck/action";

const useStyles = makeStyles(styles);

const EditPermissions = ({ match, history }) => {
  const permissionId = match.params.id;

  const [permissionName, setPermissionName] = useState("");
  const [featureAccess, setfeatureAccess] = useState({});

  const dispatch = useDispatch();

  const permissionDetails = useSelector((state) => state.permissionDetails);
  const { permission } = permissionDetails;

  useEffect(() => {
    // TODO: try edit the same just after editing any permission
    if (!permission.name || permission.id != permissionId) {
      dispatch(getPermissionDetails(permissionId));
    } else {
      setPermissionName(permission.name);
      setfeatureAccess(permission.permissionList[0]);
      console.log(JSON.stringify(permission.permissionList[0]));
    }
  }, [dispatch, match, permission]);

  const classes = useStyles();

  const handlePermissionChange = (event) => {
    setPermissionName(event.target.value);
  };

  const handleChange = (event, key) => {
    let value = event.target.value;
    setfeatureAccess((prevState) => {
      let tempState = { ...prevState };
      // console.log("updating feature access state");
      tempState[key] = value;
      return tempState;
    });
    // console.log(JSON.stringify(featureAccess));
  };

  const submitHandler = (e) => {
    console.log(`submitHandler: ${JSON.stringify(featureAccess)}`);

    e.preventDefault();

    let permissionList = { ...featureAccess };

    Object.entries(permissionList).map(
      ([key, value]) => (permissionList[key] = value.toUpperCase())
    );

    let body = {
      name: permissionName,
      permissionList: [permissionList],
      status: permission.status
    };
    dispatch(togglePermissionStatus(permission.id, body, history));
  };

  return (
    <div className={classes.permissionContainer}>
      <div className={classes.headingWrapper}>
        <h1>Edit Permissions</h1>
        <Link to="/dashboard/config/Permission">
          <Button className={classes.buttonStyles} color="primary">
            Permission List
          </Button>
        </Link>
        <Link style={{ textDecoration: "none" }} to="/dashboard/config/Permission">
          <Button
            onClick={submitHandler}
            color="primary"
            className={[classes.buttonStyles, classes.margin30]}
          >
            Update
          </Button>
        </Link>
      </div>

      <div>
        <label>
          Enter Permission Name:
          <input type="text" value={permissionName} onChange={handlePermissionChange} />
        </label>
      </div>
      <FormControl component="fieldset" className={classes.addpermission}>
        <GridContainer
          container
          // justify="left"
          justify="flex-start"
          direction="row"
          spacing={4}
          className={classes.root1}
        >
          {Object.entries(featureAccess).map(([key, value]) => (
            <GridItem key={key} className={classes.gridSpacing}>
              <FormLabel component="legend" style={{ marginBottom: "15px" }}>
                {key}
              </FormLabel>
              <select value={value} onChange={(e) => handleChange(e, key)}>
                <option value="R">R</option>
                <option value="RW">RW</option>
                <option value="RWX">RWX</option>
                <option value="NONE">None</option>
              </select>
            </GridItem>
          ))}
        </GridContainer>
      </FormControl>
    </div>
  );
};

export default EditPermissions;
