import { purple } from "@material-ui/core/colors";
const PermissionStyles = (theme) => ({
  root: {
    flexGrow: 1
  },
  paper: {
    height: 120,
    width: 125
  },
  control: {
    padding: theme.spacing(2)
  },
  cards: {
    paddingTop: "15px",
    textAlign: "center"
  }
});
export default PermissionStyles;
