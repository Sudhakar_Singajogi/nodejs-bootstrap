import React, { useState } from "react";
import { useFormik } from "formik";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import Slide from "@material-ui/core/Slide";
import { makeStyles } from "@material-ui/core/styles";
import styles from "./PermissionDialogStyles";
import AllSchemas from "utils/validationSchema";
import CustomMatInput from "components/CustomMatInput/CustomMatInput";
import EditOutlinedIcon from "@material-ui/icons/EditOutlined";
import { axiosPostData, axiosPatchData } from "utils/util";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import Divider from "@material-ui/core/Divider";
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles(styles);

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="down" ref={ref} {...props} />;
});

const PermissionDialog = ({ icon, item, updateData }) => {
  const classes = useStyles();
  const [open, setOpen] = useState(false);

  const initialValues = item
    ? { ...item }
    : {
        companyName: "",
        primaryContactName: "",
        primaryContactNumber: "",
        primaryEmail: ""
      };

  const validationSchema = AllSchemas.vendorSchema;

  const formik = useFormik({
    initialValues,
    validationSchema,
    onSubmit: async (values) => {
      values.vendorUniqueId && delete values.vendorUniqueId;
      let data = item
        ? await axiosPatchData("/api/vendors/" + item.vendorUniqueId, values)
        : await axiosPostData("/api/vendors", values);
      if (data) {
        updateData();
        setOpen(false);
      }
    }
  });

  const handleClickOpen = () => setOpen(true);

  const handleClose = () => setOpen(false);

  return (
    <>
      {icon ? (
        <EditOutlinedIcon onClick={handleClickOpen} />
      ) : (
        <Button
          variant="outlined"
          color="primary"
          size="small"
          className={classes.addVendor}
          onClick={handleClickOpen}
        >
          View Details
        </Button>
      )}

      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="form-dialog-title"
        TransitionComponent={Transition}
        keepMounted
        maxWidth="sm"
      >
        <form noValidate autoComplete="off">
          <DialogTitle id="form-dialog-title">Feature Access Levels</DialogTitle>
          <DialogContent>
            <Grid container className={classes.root} spacing={2}>
              <Grid item xs={12}>
                <Grid container justify="center" spacing={2}>
                  {initialValues &&
                    Object.entries(initialValues).map(([key, value]) => (
                      <Grid key={key} item>
                        <Paper className={[classes.paper, classes.cards]}>
                          {/* <p>{key}</p> */}
                          <Typography variant="h6" gutterBottom>
                            {key}
                          </Typography>
                          <Divider />
                          <p>{value}</p>
                        </Paper>
                      </Grid>
                    ))}
                </Grid>
              </Grid>
            </Grid>
          </DialogContent>
          <DialogActions>
            <Button onClick={handleClose} color="primary" size="small">
              Cancel
            </Button>
            {/* <Button type="submit" variant="contained" color="secondary" size="small">
              Add
            </Button> */}
          </DialogActions>
        </form>
      </Dialog>
    </>
  );
};

export default PermissionDialog;
