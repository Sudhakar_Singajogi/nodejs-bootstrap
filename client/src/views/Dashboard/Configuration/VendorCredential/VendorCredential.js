import React, { useEffect, useState } from "react";
import { ThemeProvider, makeStyles, createMuiTheme } from "@material-ui/core/styles";
import { purple } from "@material-ui/core/colors";
import Avatar from "@material-ui/core/Avatar";
import Switch from "@material-ui/core/Switch";
import Alert from "@material-ui/lab/Alert";
import Grid from "@material-ui/core/Grid";
import VendorCredentialDailog from "./VendorCredentialDailog";

import { axiosGetData, axiosPatchData } from "utils/util";
import styles from "./VendorCredentialStyles.js";
const useStyles = makeStyles(styles);

const theme = createMuiTheme({
  palette: {
    primary: purple
  }
});

const Vendor = () => {
  const classes = useStyles();
  const [vendorCredentialData, setVendorsData] = useState(null);
  const [message, setMessage] = useState(null);

  const loadData = async () => {
    try {
      const vendors = await axiosGetData("/api/vendortenantcredentials");
      if (vendors) setVendorsData(vendors.data);
    } catch (err) {
      console.log(err);
      setMessage("Error in displaying data");
    }
  };

  useEffect(() => {
    loadData();
    return () => {};
  }, []);

  const handleStatusChange = async (event, item) => {
    const vendorId = item.id;
    //console.log(`logging event value from VTCredentials: ${event.target.checked}`);
    //console.log(`logging item from VTCredentials: ${JSON.stringify(item)}`);
    item.status = event.target.checked ? "Active" : "InActive";
    let body = {
      tenantUID: item.tenantUID,
      clientId: item.clientId,
      clientSecret: item.clientSecret,
      status: item.status
    };
    //console.log(`logging request body from VTCredentials: ${JSON.stringify(body)}`);

    const loadVendors = await axiosPatchData("/api/vendortenantcredentials/" + vendorId, body);
    loadVendors && loadData();
  };

  const updateData = () => loadData();

  return (
    <ThemeProvider theme={theme}>
      <h1>
        Vendor Credential Management <VendorCredentialDailog updateData={updateData} />
      </h1>
      {message && <Alert severity="error">{message}</Alert>}
      <div className={classes.vendorsWrap}>
        {vendorCredentialData &&
          vendorCredentialData.map((item) => {
            return (
              <Grid
                container
                justify="center"
                direction="column"
                alignItems="center"
                spacing={4}
                className={classes.root}
              >
                <Grid item>
                  <Avatar className={classes.largeAvatar}>{item.tenantUID.charAt(0)}</Avatar>
                </Grid>
                <Grid item className={classes.gridSpacing}>
                  <strong>{item.tenantUID}</strong>
                </Grid>
                <Grid item className={classes.gridSpacing}>
                  {item.clientId}
                </Grid>
                <Grid item className={classes.gridSpacing}>
                  {item.clientSecret}
                </Grid>
                <Grid container direction="row" className={classes.lastSection}>
                  <Grid item xs={6}>
                    <div className={classes.paperWrap}>
                      Status <br />
                      <Switch
                        checked={item.status === "Active" ? true : false}
                        onChange={(event) => handleStatusChange(event, item)}
                        color="primary"
                        inputProps={{ "aria-label": "primary checkbox" }}
                      />
                    </div>
                  </Grid>
                  <Grid item xs={6}>
                    <div className={classes.paperWrap}>
                      <VendorCredentialDailog icon="edit" item={item} updateData={updateData} />
                    </div>
                  </Grid>
                </Grid>
              </Grid>
            );
          })}
      </div>
    </ThemeProvider>
  );
};

export default Vendor;
