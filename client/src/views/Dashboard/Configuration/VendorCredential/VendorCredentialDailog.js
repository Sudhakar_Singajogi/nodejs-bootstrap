import React, { useState } from "react";
import { useFormik } from "formik";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import Slide from "@material-ui/core/Slide";
import { makeStyles } from "@material-ui/core/styles";
import styles from "./VendorCredentialStyles.js";
import AllSchemas from "utils/validationSchema";
import CustomMatInput from "components/CustomMatInput/CustomMatInput";
import EditOutlinedIcon from "@material-ui/icons/EditOutlined";
import { axiosPostData, axiosPatchData } from "utils/util";
const useStyles = makeStyles(styles);

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="down" ref={ref} {...props} />;
});

const VendorCredentialDailog = ({ icon, item, updateData }) => {
  const classes = useStyles();
  const [open, setOpen] = useState(false);

  const initialValues = item
    ? {
        tenantUID: item.tenantUID,
        clientId: item.clientId,
        clientSecret: item.clientSecret
      }
    : {
        tenantUID: "",
        clientId: "",
        clientSecret: ""
      };

  const validationSchema = AllSchemas.createVTCSchema;
  const formik = useFormik({
    initialValues,
    validationSchema,
    onSubmit: async (values) => {
      let data = item
        ? await axiosPatchData("/api/vendortenantcredentials/" + item.id, values)
        : await axiosPostData("/api/vendortenantcredentials", values);
      if (data) {
        updateData();
        setOpen(false);
      }
    }
  });

  const handleClickOpen = () => setOpen(true);

  const handleClose = () => setOpen(false);
  return (
    <>
      {icon ? (
        <EditOutlinedIcon onClick={handleClickOpen} />
      ) : (
        <Button
          variant="contained"
          color="primary"
          size="small"
          className={classes.addVendor}
          onClick={handleClickOpen}
        >
          Add Vendor Credential
        </Button>
      )}

      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="form-dialog-title"
        TransitionComponent={Transition}
        keepMounted
        maxWidth="xs"
      >
        <form onSubmit={formik.handleSubmit} noValidate autoComplete="off">
          <DialogTitle id="form-dialog-title">Vendor Credential Details</DialogTitle>
          <DialogContent>
            <CustomMatInput label="Enter Tenant ID*" name="tenantUID" {...formik} />
            <CustomMatInput label="Enter Client ID*" name="clientId" {...formik} />
            <CustomMatInput label="Password*" name="clientSecret" {...formik} />
          </DialogContent>
          <DialogActions>
            <Button onClick={handleClose} color="primary" size="small">
              Cancel
            </Button>
            <Button type="submit" variant="contained" color="secondary" size="small">
              Add
            </Button>
          </DialogActions>
        </form>
      </Dialog>
    </>
  );
};

export default VendorCredentialDailog;
