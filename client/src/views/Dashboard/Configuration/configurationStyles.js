const configurationStyles = (theme) => ({
  configSection: {
    display: "flex",
    flexGrow: 1
  },
  configSidebar: {
    flexBasis: "250px",
    background: "#1B2430"
  },
  accordionWrapper: {
    width: "100%",
    padding: "0 20px",
    "& h1": {
      padding: "0 5px",
      color: "#8e24aa"
    }
  }
});

export default configurationStyles;
