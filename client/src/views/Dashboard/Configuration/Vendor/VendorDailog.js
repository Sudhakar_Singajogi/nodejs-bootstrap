import React, { useState } from "react";
import { useFormik } from "formik";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import Slide from "@material-ui/core/Slide";
import { makeStyles } from "@material-ui/core/styles";
import styles from "./vendorStyles.js";
import AllSchemas from "utils/validationSchema";
import CustomMatInput from "components/CustomMatInput/CustomMatInput";
import EditOutlinedIcon from "@material-ui/icons/EditOutlined";
import { axiosPostData, axiosPatchData } from "utils/util";
const useStyles = makeStyles(styles);

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="down" ref={ref} {...props} />;
});

const VendorDialog = ({ icon, item, updateData }) => {
  const classes = useStyles();
  const [open, setOpen] = useState(false);

  const initialValues = item
    ? { ...item }
    : {
        companyName: "",
        primaryContactName: "",
        primaryContactNumber: "",
        primaryEmail: ""
      };

  const validationSchema = AllSchemas.vendorSchema;
  const formik = useFormik({
    initialValues,
    validationSchema,
    onSubmit: async (values) => {
      // TODO: Vendor Id is not present in get All vendors response and hence can't send the Id in the update request
      values.vendorUniqueId && delete values.vendorUniqueId;
      // TODO: During patching, getting {"error":"\"companyName\" is not allowed"}
      values && delete values.companyName;
      console.log(`form values from VendorDialog component: ${JSON.stringify(values)}`);
      let data = item
        ? await axiosPatchData("/api/vendors/" + item.id, values)
        : await axiosPostData("/api/vendors", values);
      if (data) {
        updateData();
        setOpen(false);
      }
    }
  });

  const handleClickOpen = () => setOpen(true);

  const handleClose = () => setOpen(false);
  return (
    <>
      {icon ? (
        <EditOutlinedIcon onClick={handleClickOpen} />
      ) : (
        <Button
          variant="contained"
          color="primary"
          size="small"
          className={classes.addVendor}
          onClick={handleClickOpen}
        >
          Add Vendor
        </Button>
      )}

      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="form-dialog-title"
        TransitionComponent={Transition}
        keepMounted
        maxWidth="xs"
      >
        <form onSubmit={formik.handleSubmit} noValidate autoComplete="off">
          <DialogTitle id="form-dialog-title">Vendor Details</DialogTitle>
          <DialogContent>
            <CustomMatInput label="Company Name*" name="companyName" {...formik} />
            <CustomMatInput label="Name*" name="primaryContactName" {...formik} />
            <CustomMatInput label="Contact Number*" name="primaryContactNumber" {...formik} />
            <CustomMatInput label="Email*" name="primaryEmail" {...formik} />
          </DialogContent>
          <DialogActions>
            <Button onClick={handleClose} color="primary" size="small">
              Cancel
            </Button>
            <Button type="submit" variant="contained" color="secondary" size="small">
              Add
            </Button>
          </DialogActions>
        </form>
      </Dialog>
    </>
  );
};

export default VendorDialog;
