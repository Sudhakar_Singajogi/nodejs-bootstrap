import React, { useEffect, useState } from "react";
import { ThemeProvider, makeStyles, createMuiTheme } from "@material-ui/core/styles";
import { purple } from "@material-ui/core/colors";
import Avatar from "@material-ui/core/Avatar";
import Switch from "@material-ui/core/Switch";
import Alert from "@material-ui/lab/Alert";
import Grid from "@material-ui/core/Grid";
import VendorDailog from "./VendorDailog";
import Link from "@material-ui/core/Link";

import { axiosGetData, axiosPatchData } from "utils/util";
import styles from "./vendorStyles.js";
const useStyles = makeStyles(styles);

const theme = createMuiTheme({
  palette: {
    primary: purple
  }
});

const Vendor = () => {
  const classes = useStyles();
  const [vendorsData, setVendorsData] = useState(null);
  // console.log(`vendorsData from component: ${JSON.stringify(vendorsData)}`);
  const [message, setMessage] = useState(null);

  const loadData = async () => {
    try {
      const vendors = await axiosGetData("/api/vendors");
      if (vendors) setVendorsData(vendors.data);
    } catch (err) {
      console.log(err);
      setMessage("Error in displaying data");
    }
  };

  useEffect(() => {
    loadData();
    return () => {};
  }, []);

  const handleChange = async (event, item) => {
    // TODO: Vendor Id is not present in get All vendors response and hence can't send the Id in the update request
    const vendorId = item.id;
    delete item.vendorUniqueId;
    delete item.companyName;
    item.status = event.target.checked ? "Active" : "InActive";
    // console.log(`request body: ${JSON.stringify(item)}`);
    const loadVendors = await axiosPatchData("/api/vendors/" + vendorId, item);
    loadVendors && loadData();
  };

  const updateData = () => loadData();

  return (
    <ThemeProvider theme={theme}>
      <h1>
        Vendor Management <VendorDailog updateData={updateData} />
      </h1>
      {message && <Alert severity="error">{message}</Alert>}
      <div className={classes.vendorsWrap}>
        {vendorsData &&
          vendorsData.map((item) => {
            return (
              <Grid
                container
                justify="center"
                direction="column"
                alignItems="center"
                spacing={4}
                className={classes.root}
              >
                <Grid item>
                  <Avatar className={classes.largeAvatar}>{item.companyName.charAt(0)}</Avatar>
                </Grid>
                <Grid item className={classes.gridSpacing}>
                  <strong>{item.companyName}</strong>
                </Grid>
                <Grid item className={classes.gridSpacing}>
                  {item.primaryContactName}
                </Grid>
                <Grid item className={classes.gridSpacing}>
                  <Link href={`${item.primaryEmail}`}>{item.primaryEmail}</Link>
                </Grid>
                <Grid item className={classes.gridSpacing}>
                  {item.primaryContactNumber}
                </Grid>
                <Grid container direction="row" className={classes.lastSection}>
                  <Grid item xs={4}>
                    <div className={classes.paperWrap}>
                      Expiry:
                      <br /> <strong>Unlimited</strong>
                    </div>
                  </Grid>

                  <Grid item xs={4}>
                    <div className={classes.paperWrap}>
                      Status <br />
                      <Switch
                        checked={item.status === "Active" ? true : false}
                        onChange={(event) => handleChange(event, item)}
                        color="primary"
                        inputProps={{ "aria-label": "primary checkbox" }}
                      />
                    </div>
                  </Grid>
                  <Grid item xs={4}>
                    <div className={classes.paperWrap}>
                      <VendorDailog icon="edit" item={item} updateData={updateData} />
                    </div>
                  </Grid>
                </Grid>
              </Grid>
            );
          })}
      </div>
    </ThemeProvider>
  );
};

export default Vendor;
