import React from "react";
import { Switch } from "react-router-dom";
import ProtectedRoute from "common/approuter/ProtectedRoute";
import AutoReply from "./AccordionRoutes/AutoReply/AutoReply.js";
import ExchangeAcc from "./AccordionRoutes/ExchangeAcc/ExchangeAcc.js";
import MessageClarification from "./AccordionRoutes/MessageClarification/MessageClarification.js";
import MessageIgnore from "./AccordionRoutes/MessageIgnore/MessageIgnore.js";
import MessageSource from "./AccordionRoutes/MessageSource/MessageSource.js";
import MessageTemplate from "./AccordionRoutes/MessageTemplate/MessageTemplate.js";
import Pages from "./AccordionRoutes/Pages/Pages.js";
import ReplyTimers from "./AccordionRoutes/ReplyTimers/ReplyTimers.js";
import { Vendor } from "./Vendor";
import VendorChannel from "./VendorChannel/VendorChannel.js";
import Permission from "./Permission/Permission.js";
import AddPermission from "./Permission/AddPermission.js";
import EditPermissions from "./Permission/EditPermissions.js";
import VendorCredential from "./VendorCredential/VendorCredential.js";
import RolesScreen from "./Roles/RolesScreen.js";
import AddRole from "./Roles/AddRole.js";
import EditRole from "./Roles/EditRole.js";
import UsersScreen from "./Users/UsersScreen.js";
import AddUser from "./Users/AddUser.js";
import EditUser from "./Users/EditUser.js";

const AccordionRoutes = () => (
  <Switch>
    <ProtectedRoute exact path="/dashboard/config/vendor" component={Vendor} />
    <ProtectedRoute exact path="/dashboard/config/pages" component={Pages} />
    <ProtectedRoute path="/dashboard/config/autoReply" component={AutoReply} />
    <ProtectedRoute path="/dashboard/config/exchangeAcc" component={ExchangeAcc} />
    <ProtectedRoute path="/dashboard/config/VendorChannel" component={VendorChannel} />
    <ProtectedRoute path="/dashboard/config/VendorCredential" component={VendorCredential} />
    <ProtectedRoute path="/dashboard/config/Permission" component={Permission} />
    <ProtectedRoute
      path="/dashboard/config/messageClarification"
      component={MessageClarification}
    />
    <ProtectedRoute path="/dashboard/config/messageIgnore" component={MessageIgnore} />
    <ProtectedRoute path="/dashboard/config/messageSource" component={MessageSource} />
    <ProtectedRoute path="/dashboard/config/messageTemplate" component={MessageTemplate} />
    <ProtectedRoute path="/dashboard/config/replyTimers" component={ReplyTimers} />
    <ProtectedRoute path="/dashboard/config/AddPermission" component={AddPermission} />
    <ProtectedRoute path="/dashboard/config/EditPermissions/:id" component={EditPermissions} />
    <ProtectedRoute path="/dashboard/config/AddRole" component={AddRole} />
    <ProtectedRoute path="/dashboard/config/EditRole/:id" component={EditRole} />
    <ProtectedRoute path="/dashboard/config/Roles" component={RolesScreen} />
    <ProtectedRoute path="/dashboard/config/AddUser" component={AddUser} />
    <ProtectedRoute path="/dashboard/config/EditUser/:id" component={EditUser} />
    <ProtectedRoute path="/dashboard/config/Users" component={UsersScreen} />
  </Switch>
);

export default AccordionRoutes;
