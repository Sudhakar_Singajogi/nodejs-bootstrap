import React, { useState, useEffect } from "react";
import styles from "./RolesScreenStyles.js";
import { makeStyles } from "@material-ui/core/styles";
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import FormControl from "@material-ui/core/FormControl";
import FormLabel from "@material-ui/core/FormLabel";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import RadioGroup from "@material-ui/core/RadioGroup";
import Radio from "@material-ui/core/Radio";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import Paper from "@material-ui/core/Paper";
import Container from "@material-ui/core/Container";
import TextareaAutosize from "@material-ui/core/TextareaAutosize";

import { Link } from "react-router-dom";
// import CONFIG from "./config";
import CustomInput from "components/CustomInput/CustomInput.js";
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";

import { useDispatch, useSelector } from "react-redux";
import { createRole } from "./_duck/action";

const useStyles = makeStyles(styles);
export default function AddRole({ history }) {
  const classes = useStyles();
  const dispatch = useDispatch();
  const permissionList = useSelector((state) => state.permissionList);
  const { permissions } = permissionList;
  // console.log(`permissions from add roles: ${JSON.stringify(permissions)}`);

  const [name, setName] = useState("");
  const [option, setOption] = useState("");
  const [rolePermissionError, setRolePermissionError] = useState("");
  const [roleNameError, setRoleNameError] = useState("");
  const [roleDescriptionError, setRoleDescriptionError] = useState("");
  const handleChange = (event) => {
    event.persist();
    setRolePermissionError({});
    setOption(event.target.value);
    // console.log(event.target.value);
  };

  const [roleName, setRoleName] = useState("");
  const [roleDescription, setRoleDescription] = useState("");

  const onRoleNameChange = (event) => {
    setRoleNameError({});
    setRoleName(event.target.value);
  };
  const onRoleDescriptionChange = (event) => {
    setRoleDescriptionError({});
    setRoleDescription(event.target.value);
  };

  const validateForm = () => {
    const roleNameErr = {};
    const rolePermissionErr = {};
    const roleDesErr = {};
    let isVaild = true;

    if(roleName == ''){
      roleNameErr.requiredField = 'Role Name is required';
      isVaild = false;
    }
    if(option == ''){
      rolePermissionErr.requiredField = 'Permission is required';
      isVaild = false;
    }
    if(roleDescription == ''){
      roleDesErr.requiredField = 'Role Description is required';
      isVaild = false;
    }

    setRolePermissionError(rolePermissionErr);
    setRoleNameError(roleNameErr);
    setRoleDescriptionError(roleDesErr);
    return isVaild;

  }
  const submitHandler = (e) => {
    e.preventDefault();
    const isValid = validateForm();
    if (!isValid) {
      return;
    }
    let body = {
      name: roleName,
      description: roleDescription,
      permissionId: parseInt(option)
    };
    dispatch(createRole(body, history));
  };

  return (
    <>
      <div className={classes.permissionContainer}>
        <div className={classes.headingWrapper}>
          <h1>Add new Role</h1>

          <Link to="/dashboard/config/roles" style={{ textDecoration: "none" }}>
            <Button className={classes.buttonStyles} color="primary">
              View Roles
            </Button>
          </Link>
        </div>
        <CssBaseline />
        <main className={classes.layout} style={{ marginTop: "35px" }}>
          <Paper className={classes.paper}>
            <form noValidate>
              <Container maxWidth="sm" className={classes.container}>
                <Grid container spacing={3}>
                  <GridItem xs={8}>
                    <FormLabel component="legend" style={{ marginTop: "35px", marginBottom: "12px" }}>
                      Select Permission
                  </FormLabel>

                    <TextField
                      fullWidth
                      name="option"
                      select
                      SelectProps={{ native: true }}
                      value={option}
                      onChange={handleChange}
                      variant="outlined"
                    >
                      <option value="">Select</option>

                      {permissions &&
                        permissions.map((permission) => (
                          <option key={permission.id} value={permission.id}>
                            {permission.name}
                          </option>
                        ))}
                    </TextField>
                      {Object.keys(rolePermissionError).map((key)=>{
                        return <div style = {{color: 'red'}}>{rolePermissionError[key]}</div>
                      })
                    }
                  </GridItem>
                  <Grid item xs={8}>
                    <TextField
                      style={{ marginBottom: "15px" }}
                      fullWidth
                      required
                      onChange={onRoleNameChange}
                      value={roleName}
                      id="roleName"
                      name="roleName"
                      label="Role Name"
                    />
                    {Object.keys(roleNameError).map((key)=>{
                        return <div style = {{color: 'red'}}>{roleNameError[key]}</div>
                      })
                    }
                  </Grid>
                  <Grid item xs={8}>
                    <TextareaAutosize
                      className={classes.textArea}
                      onChange={onRoleDescriptionChange}
                      value={roleDescription}
                      placeholder="Role Description"
                      rows={3}
                    />
                    {Object.keys(roleDescriptionError).map((key)=>{
                        return <div style = {{color: 'red'}}>{roleDescriptionError[key]}</div>
                      })
                    }
                  </Grid>

                  <Grid item xs={8} style={{ marginBottom: "35px" }}>
                    <Link style={{ textDecoration: "none" }}>
                      <Button
                        color="primary"
                        variant="contained"
                        className={classes.buttonStyles}
                        onClick={submitHandler}
                      >
                        Add Role
                    </Button>
                    </Link>
                  </Grid>
                </Grid>
              </Container>
            </form>
          </Paper>
        </main>
      </div>
    </>
  );
}
