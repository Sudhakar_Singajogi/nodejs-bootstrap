import {
  ROLES_LIST_REQUEST,
  ROLES_LIST_SUCCESS,
  ROLES_LIST_FAIL,
  ROLES_CREATE_FAIL,
  ROLES_CREATE_REQUEST,
  ROLES_CREATE_SUCCESS,
  ROLE_DETAILS_FAIL,
  ROLE_DETAILS_REQUEST,
  ROLE_DETAILS_SUCCESS,
  ROLE_UPDATE_FAIL,
  ROLE_UPDATE_REQUEST,
  ROLE_UPDATE_SUCCESS
} from "./type";

const INITIAL_STATE = {
  roles: []
};

export const rolesListReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case ROLES_LIST_REQUEST:
      return { loading: true, roles: [] };
    case ROLES_LIST_SUCCESS:
      return {
        loading: false,
        roles: action.payload
      };
    case ROLES_LIST_FAIL:
      return { loading: false, error: action.payload };
    default:
      return state;
  }
};

export const roleCreateReducer = (state = {}, action) => {
  switch (action.type) {
    case ROLES_CREATE_REQUEST:
      return { loading: true };
    case ROLES_CREATE_SUCCESS:
      return { loading: false, error: "", role: action.payload };
    case ROLES_CREATE_FAIL:
      return { loading: false, error: action.payload };
    default:
      return state;
  }
};

export const roleDetailsReducer = (state = { role: {} }, action) => {
  switch (action.type) {
    case ROLE_DETAILS_REQUEST:
      return { ...state, loading: true };
    case ROLE_DETAILS_SUCCESS:
      return {
        loading: false,
        role: action.payload
      };
    case ROLE_DETAILS_FAIL:
      return { loading: false, error: action.payload };
    default:
      return state;
  }
};

export const roleUpdateReducer = (state = { role: {} }, action) => {
  switch (action.type) {
    case ROLE_UPDATE_REQUEST:
      return { loading: true };
    case ROLE_UPDATE_SUCCESS:
      return { loading: false, role: action.payload };
    case ROLE_UPDATE_FAIL:
      return { loading: false, error: action.payload };
    default:
      return state;
  }
};
