import {
  ROLES_LIST_REQUEST,
  ROLES_LIST_SUCCESS,
  ROLES_LIST_FAIL,
  ROLES_CREATE_FAIL,
  ROLES_CREATE_REQUEST,
  ROLES_CREATE_SUCCESS,
  ROLE_DETAILS_FAIL,
  ROLE_DETAILS_REQUEST,
  ROLE_DETAILS_SUCCESS,
  ROLE_UPDATE_FAIL,
  ROLE_UPDATE_REQUEST,
  ROLE_UPDATE_SUCCESS
} from "./type";

import { axiosGetData, axiosPostData, axiosPatchData } from "utils/util";

export const listRoles = () => async (dispatch) => {
  try {
    dispatch({
      type: ROLES_LIST_REQUEST
    });
    const { data } = await axiosGetData("/api/roles");

    dispatch({
      type: ROLES_LIST_SUCCESS,
      payload: data
    });
  } catch (error) {
    dispatch({
      type: ROLES_LIST_FAIL,
      payload:
        error.response && error.response.data.message ? error.response.data.message : error.message
    });
  }
};
export const createRole = (body, history) => async (dispatch) => {
  console.log(`print create roles body from action: ${JSON.stringify(body)}`);
  try {
    dispatch({
      type: ROLES_CREATE_REQUEST
    });

    const { data } = await axiosPostData(`/api/roles`, body);

    dispatch({
      type: ROLES_CREATE_SUCCESS,
      payload: data
    });

    history.push("/dashboard/config/roles");
  } catch (error) {
    dispatch({
      type: ROLES_CREATE_FAIL,
      payload:
        error.response && error.response.data.message ? error.response.data.message : error.message
    });
  }
};
export const getRoleDetails = (id) => async (dispatch) => {
  try {
    dispatch({
      type: ROLE_DETAILS_REQUEST
    });

    const { data } = await axiosGetData(`/api/roles/${id}`);

    dispatch({
      type: ROLE_DETAILS_SUCCESS,
      payload: data
    });
  } catch (error) {
    dispatch({
      type: ROLE_DETAILS_FAIL,
      payload:
        error.response && error.response.data.message ? error.response.data.message : error.message
    });
  }
};

export const updateRole = (id, body, history = null) => async (dispatch) => {
  try {
    dispatch({
      type: ROLE_UPDATE_REQUEST
    });
    const { data } = await axiosPatchData(`/api/roles/${id}`, body);

    dispatch({
      type: ROLE_UPDATE_SUCCESS,
      payload: data
    });

    // update client side
    // dispatch({
    //   type: ROLE_UPDATE_FAIL,
    //   payload: permission.data
    // });

    if (history) {
      history.push("/dashboard/config/roles");
    }
  } catch (error) {
    dispatch({
      type: ROLE_UPDATE_FAIL,
      payload:
        error.response && error.response.data.message ? error.response.data.message : error.message
    });
  }
};
