import React, { useState, useEffect } from "react";
import styles from "./RolesScreenStyles.js";
import { makeStyles } from "@material-ui/core/styles";
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import FormControl from "@material-ui/core/FormControl";
import FormLabel from "@material-ui/core/FormLabel";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import RadioGroup from "@material-ui/core/RadioGroup";
import Radio from "@material-ui/core/Radio";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import Paper from "@material-ui/core/Paper";
import Container from "@material-ui/core/Container";
import TextareaAutosize from "@material-ui/core/TextareaAutosize";

import { Link } from "react-router-dom";
// import CONFIG from "./config";
import CustomInput from "components/CustomInput/CustomInput.js";
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";

import { useDispatch, useSelector } from "react-redux";
import { getRoleDetails, updateRole } from "./_duck/action";

const useStyles = makeStyles(styles);
export default function EditRole({ match, history }) {
  const roleId = match.params.id;

  const classes = useStyles();
  const dispatch = useDispatch();
  const permissionList = useSelector((state) => state.permissionList);
  const { permissions } = permissionList;
  // console.log(`permissions from add roles: ${JSON.stringify(permissions)}`);

  const [roleName, setRoleName] = useState("");
  const [roleDescription, setRoleDescription] = useState("");
  const [option, setOption] = useState("");
  const [status, setStatus] = useState("");

  const roleDetails = useSelector((state) => state.roleDetails);
  const { role } = roleDetails;

  useEffect(() => {
    if (!role.name || role.id != roleId) {
      dispatch(getRoleDetails(roleId));
    } else {
      setRoleName(role.name);
      setRoleDescription(role.description);
      setOption(role.permissionId);
      setStatus(role.status);
    }
  }, [dispatch, match, role]);

  const handleChange = (event) => {
    event.persist();
    setOption(event.target.value);
  };
  const handleStatusChange = (event) => {
    event.persist();
    setStatus(event.target.value);
  };

  const onRoleNameChange = (event) => {
    setRoleName(event.target.value);
  };
  const onRoleDescriptionChange = (event) => {
    setRoleDescription(event.target.value);
  };

  const submitHandler = (e) => {
    e.preventDefault();

    let body = {
      status: status
    };

    dispatch(updateRole(roleId, body, history));
  };

  return (
    <>
      <div className={classes.permissionContainer}>
        <div className={classes.headingWrapper}>
          <h1>Edit Role</h1>
          <Link to="/dashboard/config/roles" style={{ textDecoration: "none" }}>
            <Button className={classes.buttonStyles} color="primary">
              View Roles
            </Button>
          </Link>
        </div>
        <CssBaseline />
        <main className={classes.layout} style={{ marginTop: "35px" }}>
          <Paper className={classes.paper}>
            <Container maxWidth="sm" className={classes.container}>
              <Grid container spacing={3}>
                <GridItem xs={8}>
                  <FormLabel component="legend" style={{ marginTop: "35px", marginBottom: "12px" }}>
                    Select Permission
                  </FormLabel>

                  <TextField
                    fullWidth
                    name="option"
                    select
                    SelectProps={{ native: true }}
                    value={option}
                    onChange={handleChange}
                    variant="outlined"
                  >
                    <option value="">Select</option>

                    {permissions &&
                      permissions.map((permission) => (
                        <option key={permission.id} value={permission.id}>
                          {permission.name}
                        </option>
                      ))}
                  </TextField>
                </GridItem>

                <Grid item xs={8}>
                  <TextField
                    style={{ marginBottom: "15px" }}
                    fullWidth
                    required
                    onChange={onRoleNameChange}
                    value={roleName}
                    id="roleName"
                    name="roleName"
                    label="Role Name"
                  />
                </Grid>
                <Grid item xs={8}>
                  <TextareaAutosize
                    className={classes.textArea}
                    onChange={onRoleDescriptionChange}
                    value={roleDescription}
                    placeholder="Role Description"
                    rows={3}
                    // style={{ marginBottom: "35px" }}
                  />
                </Grid>

                <GridItem xs={8}>
                  <FormLabel component="legend" style={{ marginBottom: "12px" }}>
                    Status
                  </FormLabel>

                  <TextField
                    fullWidth
                    name="status"
                    select
                    SelectProps={{ native: true }}
                    value={status}
                    onChange={handleStatusChange}
                    variant="outlined"
                  >
                    <option value="Active">Active</option>
                    <option value="InActive">In Active</option>
                  </TextField>
                </GridItem>

                <Grid item xs={8} style={{ marginBottom: "35px" }}>
                  <Link style={{ textDecoration: "none" }}>
                    <Button
                      color="primary"
                      variant="contained"
                      className={classes.buttonStyles}
                      onClick={submitHandler}
                    >
                      Update
                    </Button>
                  </Link>
                </Grid>
              </Grid>
            </Container>
          </Paper>
        </main>
      </div>
    </>
  );
}
