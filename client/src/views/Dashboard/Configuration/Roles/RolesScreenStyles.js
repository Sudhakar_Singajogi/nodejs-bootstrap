import { purple } from "@material-ui/core/colors";
const RolesScreenStyles = (theme) => ({
  vendorsWrap: {
    display: "flex",
    flexWrap: "wrap"
  },
  root: {
    width: "100%",
    margin: 0,
    marginBottom: "20px",
    minHeight: 250,
    boxShadow: "0px 1px 5px #CCC",
    transition: "all .5s",
    [theme.breakpoints.up("sm")]: {
      width: "50%"
    },

    [theme.breakpoints.up("lg")]: {
      width: "25%"
    },
    "& strong": {
      color: "#8e24aa"
    },
    "&:hover": {
      transform: "scale(1.10)",
      background: "#fafafa"
    }
  },

  buttonStyles: {
    marginTop: "24px",
    background: "#8e24aa",
    color: "#fff",
    "&:hover": {
      backgroundColor: "#8e24aa",
      color: "#fff"
    }
  },

  headingWrapper: {
    width: "100%",
    display: "flex",
    "& h1": {
      flexGrow: "1"
    }
  },

  bullet: {
    display: "inline-block",
    margin: "0 2px",
    transform: "scale(0.8)"
  },
  title: {
    fontSize: 14
  },
  pos: {
    marginBottom: 12
  },
  addRole: {
    float: "right"
  },
  largeAvatar: {
    width: theme.spacing(8),
    height: theme.spacing(8),
    color: "#fff",
    backgroundColor: purple[700]
  },

  textArea: {
    ...theme.typography.body1,
    // border: "none",
    // outline: "none",
    // resize: "none",
    width: "100%"
  },

  lastSection: {
    borderTop: "1px solid #EEE",
    width: "100%",
    alignItems: "center",
    marginTop: "10px"
  },
  paperWrap: {
    padding: theme.spacing(0.5),
    borderRight: "1px solid #EEE",
    fontSize: "13px",
    textAlign: "center",
    color: theme.palette.text.secondary
  },
  gridSpacing: {
    padding: "5px !important",
    textAlign: "center"
  }
});

export default RolesScreenStyles;
