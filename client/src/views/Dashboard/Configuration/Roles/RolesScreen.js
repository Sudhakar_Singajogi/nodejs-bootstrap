import React, { useEffect, useState } from "react";
import { ThemeProvider, makeStyles, createMuiTheme } from "@material-ui/core/styles";
import { purple } from "@material-ui/core/colors";
import Avatar from "@material-ui/core/Avatar";
import Switch from "@material-ui/core/Switch";
import Button from "@material-ui/core/Button";
import Checkbox from "@material-ui/core/Checkbox";
import Alert from "@material-ui/lab/Alert";
import Grid from "@material-ui/core/Grid";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
// import Title from "./Title";
// import Link from "@material-ui/core/Link";
import { Link } from "react-router-dom";
import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";
import IconButton from "@material-ui/core/IconButton";
import Badge from "@material-ui/core/Badge";
import Container from "@material-ui/core/Container";
// import Grid from '@material-ui/core/Grid';
import Paper from "@material-ui/core/Paper";
import Box from "@material-ui/core/Box";
import { useDispatch, useSelector } from "react-redux";
import { listRoles } from "./_duck/action";
import styles from "./RolesScreenStyles.js";
import EditOutlinedIcon from "@material-ui/icons/EditOutlined";
import EditIcon from "@material-ui/icons/Edit";
import { axiosPatchData } from "utils/util";

const useStyles = makeStyles(styles);

const theme = createMuiTheme({
  palette: {
    primary: purple
  }
});

const RolesScreen = () => {
  const classes = useStyles();
  const dispatch = useDispatch();

  const [message, setMessage] = useState(null);
  const rolesList = useSelector((state) => state.rolesList);
  const { roles, error, loading } = rolesList;
  // console.log(`roles from functiona body: ${JSON.stringify(roles)} `);
  const loadData = async () => {
    try {
      dispatch(listRoles());
    } catch (err) {
      setMessage("Error in displaying data");
    }
  };

  const handleStatusChange = async (event, role) => {
    const roleId = role.id;
    let body = {
      status: event.target.checked ? "Active" : "InActive"
    };
    console.log(`request body: ${JSON.stringify(body)}`);
    const loadRoles = await axiosPatchData("/api/roles/" + roleId, body);
    loadRoles && loadData();
  };

  useEffect(() => {
    loadData();
  }, []);

  return (
    <ThemeProvider theme={theme}>
      <h1>
        Roles
        <Link to="/dashboard/config/AddRole">
          <Button
            color="primary"
            style={{ right: "40px", top: "60px", position: "absolute" }}
            className={classes.buttonStyles}
          >
            Add Role
          </Button>
        </Link>
      </h1>
      {message && <Alert severity="error">{message}</Alert>}
      <div className={classes.vendorsWrap}>
        <div className={classes.appBarSpacer} />
        <Container maxWidth="md" className={classes.container} style={{ marginTop: "35px" }}>
          <Grid container spacing={3}>
            <Grid item xs={12}>
              <Paper className={classes.paper}>
                <div>
                  <Table>
                    <TableHead>
                      <TableRow>
                        {/* <TableCell>Role Id</TableCell> */}
                        <TableCell>Role Name</TableCell>
                        <TableCell>Role Description</TableCell>
                        <TableCell>Permission Name</TableCell>
                        <TableCell>Status</TableCell>
                        <TableCell align="right">Edit Role</TableCell>
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      {roles &&
                        roles.map((role) => (
                          <TableRow key={role.id}>
                            {/* <TableCell>{role.id}</TableCell> */}
                            <TableCell>{role.name}</TableCell>
                            <TableCell>{role.description}</TableCell>
                            <TableCell>{role.Permissions.name}</TableCell>
                            {/* <TableCell>{role.status}</TableCell> */}
                            <TableCell>
                              {
                                <Checkbox
                                  checked={role.status === "Active" ? true : false}
                                  onChange={(event) => handleStatusChange(event, role)}
                                  color="primary"
                                  inputProps={{ "aria-label": "secondary checkbox" }}
                                />
                              }
                            </TableCell>
                            <TableCell align="right">
                              <Link color="primary" to={`/dashboard/config/EditRole/${role.id}`}>
                                <EditOutlinedIcon />
                              </Link>
                            </TableCell>
                          </TableRow>
                        ))}
                    </TableBody>
                  </Table>
                </div>
              </Paper>
            </Grid>
          </Grid>
        </Container>
      </div>
    </ThemeProvider>
  );
};

export default RolesScreen;
