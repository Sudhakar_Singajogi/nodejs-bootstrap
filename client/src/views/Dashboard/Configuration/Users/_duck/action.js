import {
  USERS_LIST_REQUEST,
  USERS_LIST_SUCCESS,
  USERS_LIST_FAIL,
  USERS_CREATE_FAIL,
  USERS_CREATE_REQUEST,
  USERS_CREATE_SUCCESS,
  USER_DETAILS_FAIL,
  USER_DETAILS_REQUEST,
  USER_DETAILS_SUCCESS,
  USER_UPDATE_FAIL,
  USER_UPDATE_REQUEST,
  USER_UPDATE_SUCCESS
} from "./type";

import { axiosGetData, axiosPostData, axiosPatchData } from "utils/util";

export const listUsers = () => async (dispatch) => {
  try {
    dispatch({
      type: USERS_LIST_REQUEST
    });
    const { data } = await axiosGetData("/api/users");

    dispatch({
      type: USERS_LIST_SUCCESS,
      payload: data
    });
  } catch (error) {
    dispatch({
      type: USERS_LIST_FAIL,
      payload:
        error.response && error.response.data.message ? error.response.data.message : error.message
    });
  }
};
export const createUser = (body, history) => async (dispatch) => {
  console.log(`print create users body from action: ${JSON.stringify(body)}`);
  try {
    dispatch({
      type: USERS_CREATE_REQUEST
    });

    const { data } = await axiosPostData(`/api/users`, body);

    dispatch({
      type: USERS_CREATE_SUCCESS,
      payload: data
    });

    history.push("/dashboard/config/users");
  } catch (error) {
    dispatch({
      type: USERS_CREATE_FAIL,
      payload:
        error.response && error.response.data.message ? error.response.data.message : error.message
    });
  }
};
export const getUserDetails = (id) => async (dispatch) => {
  try {
    dispatch({
      type: USER_DETAILS_REQUEST
    });

    const { data } = await axiosGetData(`/api/users/${id}`);

    dispatch({
      type: USER_DETAILS_SUCCESS,
      payload: data
    });
  } catch (error) {
    dispatch({
      type: USER_DETAILS_FAIL,
      payload:
        error.response && error.response.data.message ? error.response.data.message : error.message
    });
  }
};

export const updateUser = (id, body, history = null) => async (dispatch) => {
  try {
    dispatch({
      type: USER_UPDATE_REQUEST
    });
    const { data } = await axiosPatchData(`/api/users/${id}`, body);

    dispatch({
      type: USER_UPDATE_SUCCESS,
      payload: data
    });

    if (history) {
      history.push("/dashboard/config/users");
    }
  } catch (error) {
    dispatch({
      type: USER_UPDATE_FAIL,
      payload:
        error.response && error.response.data.message ? error.response.data.message : error.message
    });
  }
};
