import React, { useEffect, useState } from "react";
import { ThemeProvider, makeStyles, createMuiTheme } from "@material-ui/core/styles";
import { purple } from "@material-ui/core/colors";
import Avatar from "@material-ui/core/Avatar";
import Switch from "@material-ui/core/Switch";
import Button from "@material-ui/core/Button";
import Checkbox from "@material-ui/core/Checkbox";
import Alert from "@material-ui/lab/Alert";
import Grid from "@material-ui/core/Grid";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
// import Title from "./Title";
// import Link from "@material-ui/core/Link";
import { Link } from "react-router-dom";
import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";
import IconButton from "@material-ui/core/IconButton";
import Badge from "@material-ui/core/Badge";
import Container from "@material-ui/core/Container";
// import Grid from '@material-ui/core/Grid';
import Paper from "@material-ui/core/Paper";
import Box from "@material-ui/core/Box";
import { useDispatch, useSelector } from "react-redux";
import { listUsers } from "./_duck/action";
import styles from "./UsersScreenStyles.js";
import EditOutlinedIcon from "@material-ui/icons/EditOutlined";
import EditIcon from "@material-ui/icons/Edit";
import { axiosPatchData } from "utils/util";

const useStyles = makeStyles(styles);

const theme = createMuiTheme({
  palette: {
    primary: purple
  }
});

const UsersScreen = () => {
  const classes = useStyles();
  const dispatch = useDispatch();

  const [message, setMessage] = useState(null);
  const usersList = useSelector((state) => state.usersList);
  const { users, error, loading } = usersList;
  // console.log(`roles from functiona body: ${JSON.stringify(roles)} `);
  const loadData = async () => {
    try {
      dispatch(listUsers());
    } catch (err) {
      setMessage("Error in displaying data");
    }
  };

  const handleStatusChange = async (event, user) => {
    const userId = user.id;
    let body = {
      status: event.target.checked ? "Active" : "InActive"
    };
    console.log(`request body: ${JSON.stringify(body)}`);
    const loadUsers = await axiosPatchData("/api/users/" + userId, body);
    loadUsers && loadData();
  };

  useEffect(() => {
    loadData();
  }, []);

  return (
    <ThemeProvider theme={theme}>
      <h1>
        Users
        {/* <Button variant="contained" color="primary" size="small" className={classes.addRole}>
          Add Role
        </Button> */}
        <Link to="/dashboard/config/AddUser">
          <Button
            color="primary"
            style={{ right: "40px", top: "60px", position: "absolute" }}
            className={classes.buttonStyles}
          >
            Add User
          </Button>
        </Link>
      </h1>
      {message && <Alert severity="error">{message}</Alert>}
      <div className={classes.vendorsWrap}>
        {/* <main className={classes.content}> */}
        <div className={classes.appBarSpacer} />
        <Container style={{ marginTop: "35px" }} className={classes.container}>
          <Grid container spacing={3}>
            {/* Roles */}
            <Grid item xs={12}>
              <Paper className={classes.paper}>
                <div>
                  <Table>
                    <TableHead>
                      <TableRow>
                        <TableCell>Staff Id</TableCell>
                        <TableCell>User Name</TableCell>
                        <TableCell>User email</TableCell>
                        <TableCell>User Role</TableCell>
                        <TableCell>Status</TableCell>
                        <TableCell align="right">Edit User</TableCell>
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      {users &&
                        users.map((user) => (
                          <TableRow key={user.id}>
                            <TableCell>{user.id}</TableCell>
                            <TableCell>{user.userName}</TableCell>
                            <TableCell>{user.email}</TableCell>
                            <TableCell>{user.Role.name}</TableCell>
                            <TableCell>
                              {
                                <Checkbox
                                  checked={user.status === "Active" ? true : false}
                                  onChange={(event) => handleStatusChange(event, user)}
                                  color="primary"
                                  inputProps={{ "aria-label": "secondary checkbox" }}
                                />
                              }
                            </TableCell>
                            <TableCell align="right">
                              <Link color="primary" to={`/dashboard/config/EditUser/${user.id}`}>
                                <EditOutlinedIcon />
                              </Link>
                            </TableCell>
                          </TableRow>
                        ))}
                    </TableBody>
                  </Table>
                </div>
              </Paper>
            </Grid>
          </Grid>
          {/* <Box pt={4}>Copyright</Box> */}
        </Container>
        {/* </main> */}
      </div>
    </ThemeProvider>
  );
};

export default UsersScreen;
