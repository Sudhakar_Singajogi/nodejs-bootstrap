import React, { useState, useEffect } from "react";
import styles from "./UsersScreenStyles.js";
import { makeStyles } from "@material-ui/core/styles";
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import FormControl from "@material-ui/core/FormControl";
import FormLabel from "@material-ui/core/FormLabel";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import RadioGroup from "@material-ui/core/RadioGroup";
import Radio from "@material-ui/core/Radio";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import Paper from "@material-ui/core/Paper";
import Container from "@material-ui/core/Container";
import TextareaAutosize from "@material-ui/core/TextareaAutosize";

import { Link } from "react-router-dom";
// import CONFIG from "./config";
import CustomInput from "components/CustomInput/CustomInput.js";
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";

import { useDispatch, useSelector } from "react-redux";
import { createUser } from "./_duck/action";

const useStyles = makeStyles(styles);
export default function AddUser({ history }) {
  const classes = useStyles();
  const dispatch = useDispatch();
  const rolesList = useSelector((state) => state.rolesList);
  const { roles } = rolesList;

  const [userName, setUserName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [option, setOption] = useState("");

  const handleChange = (event) => {
    event.persist();
    setOption(event.target.value);
  };
  const onUserNameChange = (event) => {
    setUserName(event.target.value);
  };
  const onEmailChange = (event) => {
    setEmail(event.target.value);
  };
  const onPasswordChange = (event) => {
    setPassword(event.target.value);
  };

  const submitHandler = (e) => {
    e.preventDefault();
    let body = {
      userName,
      email,
      password,
      RoleId: option
    };
    dispatch(createUser(body, history));
  };

  return (
    <>
      <div className={classes.permissionContainer}>
        <div className={classes.headingWrapper}>
          <h1>Add new User</h1>

          <Link to="/dashboard/config/users" style={{ textDecoration: "none" }}>
            <Button className={classes.buttonStyles} color="primary">
              View Users
            </Button>
          </Link>
        </div>
        <CssBaseline />
        <main className={classes.layout} style={{ marginTop: "25px" }}>
          <Paper className={classes.paper}>
            <Container maxWidth="sm" className={classes.container}>
              <Grid container spacing={3}>
                <GridItem xs={8}>
                  <FormLabel component="legend" style={{ marginTop: "35px", marginBottom: "12px" }}>
                    Select Role
                  </FormLabel>

                  <TextField
                    fullWidth
                    name="option"
                    select
                    SelectProps={{ native: true }}
                    value={option}
                    onChange={handleChange}
                    variant="outlined"
                  >
                    <option value="">Select</option>

                    {roles &&
                      roles.map((role) => (
                        <option key={role.id} value={role.id}>
                          {role.name}
                        </option>
                      ))}
                  </TextField>
                </GridItem>

                <Grid item xs={8}>
                  <TextField
                    style={{ marginBottom: "15px" }}
                    fullWidth
                    required
                    onChange={onUserNameChange}
                    value={userName}
                    id="userName"
                    name="userName"
                    label="User Name"
                  />
                </Grid>
                <Grid item xs={8}>
                  <TextField
                    style={{ marginBottom: "15px" }}
                    fullWidth
                    required
                    onChange={onEmailChange}
                    value={email}
                    id="email"
                    name="email"
                    label="Email"
                    type="email"
                  />
                </Grid>

                <Grid item xs={8}>
                  <TextField
                    fullWidth
                    required
                    onChange={onPasswordChange}
                    type="password"
                    value={password}
                    id="password"
                    name="password"
                    label="Password"
                  />
                </Grid>

                <Grid item xs={8} style={{ marginBottom: "35px" }}>
                  <Link style={{ textDecoration: "none" }}>
                    <Button
                      color="primary"
                      variant="contained"
                      className={classes.buttonStyles}
                      onClick={submitHandler}
                    >
                      Add User
                    </Button>
                  </Link>
                </Grid>
              </Grid>
            </Container>
          </Paper>
        </main>
      </div>
    </>
  );
}
