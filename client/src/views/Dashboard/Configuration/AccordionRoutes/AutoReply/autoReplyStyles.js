const autoReplyStyles = (theme) => ({
  margin40: {
    marginLeft: "40px"
  },
  margin30: {
    marginLeft: "30px"
  }
});

export default autoReplyStyles;
