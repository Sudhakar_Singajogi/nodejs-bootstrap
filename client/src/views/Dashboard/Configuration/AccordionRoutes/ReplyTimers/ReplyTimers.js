import React from "react";
import styles from "./replyTimersStyles.js";
import { makeStyles } from "@material-ui/core/styles";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";
const useStyles = makeStyles(styles);
export default function ReplyTimers() {
  const classes = useStyles();
  const [value1, setVal] = React.useState("");
  const handleChange = (event) => {
    setVal(event.target.value);
  };
  return (
    <Card className={classes.margin40}>
      <div className={classes.margin30}>
        <h2>Message Reply Timers</h2>
        <InputLabel id="demo-simple-select-label">Default reply deadline:</InputLabel>
        <Select
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          value={value1}
          onChange={handleChange}
          className={classes.selectStyles}
        >
          <MenuItem value={10}>value2</MenuItem>
          <MenuItem value={20}>value3</MenuItem>
          <MenuItem value={30}>value4</MenuItem>
        </Select>{" "}
        hours
        <p className={classes.cardCategoryWhite}>
          {" "}
          <Button color="primary">Add New rule</Button>
        </p>
      </div>
    </Card>
  );
}
