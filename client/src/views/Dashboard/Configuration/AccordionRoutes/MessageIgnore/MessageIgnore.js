import React from "react";
import styles from "./messageIgnoreStyles.js";
import { makeStyles } from "@material-ui/core/styles";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
// core components
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";

const useStyles = makeStyles(styles);
export default function MessageIgnore() {
  const classes = useStyles();
  return (
    <Card className={classes.margin40}>
      <div className={classes.margin30}>
        <h2>Ignore rules</h2>
        <GridContainer>
          <GridItem xs={12} sm={12} md={6}>
            <p>Ignored messages are periodically deleted if they are over 2 weeks old</p>
          </GridItem>

          <GridItem xs={12} sm={12} md={6}>
            <p>
              <Button color="primary" className={classes.marginRgt20}>
                Add Default rule
              </Button>

              <Button color="primary">Apply rules now</Button>
            </p>
          </GridItem>
        </GridContainer>

        <Button color="primary">Add new rule</Button>
      </div>
    </Card>
  );
}
