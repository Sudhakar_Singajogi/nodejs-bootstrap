const messageIgnoreStyles = (theme) => ({
  margin40: {
    marginLeft: "40px"
  },
  margin30: {
    marginLeft: "30px"
  },
  marginRgt20: {
    marginRight: "20px"
  }
});

export default messageIgnoreStyles;
