const messageClarificationStyles = (theme) => ({
  margin40: {
    marginLeft: "40px"
  },

  clarificationWrapper: {
    "& label": {
      float: "left"
    },
    marginLeft: "30px"
  },
  inputStyles: {
    paddingTop: 0,
    marginLeft: "30px",
    width: "50%"
  },
  selectStyles: {
    width: "150px",
    marginLeft: "50px"
  }
});

export default messageClarificationStyles;
