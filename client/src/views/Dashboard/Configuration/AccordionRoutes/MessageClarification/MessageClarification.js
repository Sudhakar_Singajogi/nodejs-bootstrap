import React from "react";
import styles from "./messageClarificationStyles.js";
import { makeStyles } from "@material-ui/core/styles";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CustomInput from "components/CustomInput/CustomInput.js";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";
const useStyles = makeStyles(styles);
export default function MessageClarification() {
  const classes = useStyles();
  const [value1, setVal] = React.useState("");
  const handleChange = (event) => {
    setVal(event.target.value);
  };

  return (
    <Card className={classes.margin40}>
      <div className={classes.clarificationWrapper}>
        <h2>Add new type of classification rule</h2>
        <p>
          There should be no need for you to configure Amazon and Ebay sources here as these are
          recognized by the system
        </p>
        <div>
          <label for="first-name">If subject contains:</label>
          <CustomInput classes="inputStyles" id="first-name" formControlProps={{}} />
        </div>
        <div>
          <InputLabel id="demo-simple-select-label">Classify it as:</InputLabel>
          <Select
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            value={value1}
            onChange={handleChange}
            className={classes.selectStyles}
          >
            <MenuItem value={10}>value2</MenuItem>
            <MenuItem value={20}>value3</MenuItem>
            <MenuItem value={30}>value4</MenuItem>
          </Select>
        </div>

        <p className={classes.cardCategoryWhite}>
          {" "}
          <Button color="primary">Add New rule</Button>
        </p>
      </div>
    </Card>
  );
}
