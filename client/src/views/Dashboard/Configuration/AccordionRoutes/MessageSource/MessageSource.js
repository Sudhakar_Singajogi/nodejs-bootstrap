import React from "react";
import styles from "./messageSourceStyles.js";
import { makeStyles } from "@material-ui/core/styles";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";

const useStyles = makeStyles(styles);
export default function MessageSource() {
  const classes = useStyles();
  return (
    <Card className={classes.margin40}>
      <div className={classes.margin30}>
        <h2>Message Source rules</h2>
        <p>
          There should be no need for you to configure Amazon and Ebay sources here as these are
          recognized by the system
        </p>
        <p className={classes.cardCategoryWhite}>
          {" "}
          <Button color="primary">Add New rule</Button>
        </p>
      </div>
    </Card>
  );
}
