import React from "react";
import styles from "./messageTemplateStyles.js";
import { makeStyles } from "@material-ui/core/styles";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";

const useStyles = makeStyles(styles);
export default function MessageTemplate() {
  const classes = useStyles();
  return (
    <Card className={classes.margin40}>
      <div className={classes.margin30}>
        <h2>Message Templates</h2>
        <p className={classes.cardCategoryWhite}>
          {" "}
          <Button color="primary">+ Add new Template</Button>
        </p>
      </div>
    </Card>
  );
}
