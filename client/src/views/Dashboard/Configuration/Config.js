export default {
  users: [
    {
      heading: "General",
      panelbool: true,
      subHeadings: [
        {
          name: "Vendor",
          linkTo: "/dashboard/config/vendor",
          icon: "file-alt"
        },
        {
          name: "Vendor  Channel",
          linkTo: "/dashboard/config/VendorChannel",
          icon: "file-alt"
        },
        {
          name: "Vendor Credential",
          linkTo: "/dashboard/config/VendorCredential",
          icon: "file-alt"
        },
        {
          name: "Permission",
          linkTo: "/dashboard/config/Permission",
          icon: "file-alt"
        },
        {
          name: "Role",
          linkTo: "/dashboard/config/Roles",
          icon: "file-alt"
        },
        {
          name: "User",
          linkTo: "/dashboard/config/Users",
          icon: "file-alt"
        }
      ]
    },
    {
      heading: "Message Center",
      panelbool: true,
      subHeadings: [
        {
          name: "Exchange Accounts",
          subheadingPanelbool: false,
          linkTo: "/dashboard/config/exchangeAcc",
          icon: "user"
        },

        {
          name: "Message Templates",
          subheadingPanelbool: false,
          linkTo: "/messageTemplate",
          icon: "tasks"
        },
        {
          name: "Reply timers",
          subheadingPanelbool: false,
          linkTo: "/replyTimers",
          icon: "calendar"
        },
        {
          name: "Rules",
          subheadingPanelbool: true,
          // TODO: Failed prop type: The prop `to` is marked as required in `Link`, but its value is `undefined`.
          // linkTo: "",
          subHeadingsChild: [
            {
              name: "Auto Reply rules",
              linkTo: "/autoReply"
            },
            {
              name: "Message Source rule",
              linkTo: "/messageSource"
            },
            {
              name: "Message Classification rules",
              linkTo: "/messageClarification"
            },
            {
              name: "Message Ignore rules",
              linkTo: "/messageIgnore"
            }
          ]
        }
      ]
    }
  ]
};
