import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import CONFIG from "./Config";
import ConfigAccordion from "./ConfigAccordion.js";
import ConfigurationRoutes from "./ConfigurationRoutes";
import styles from "./configurationStyles.js";
let users = CONFIG.users;

const useStyles = makeStyles(styles);

export default function ConfigurationPage() {
  const classes = useStyles();
  const [expanded, setExpanded] = useState();
  return (
    <div className={classes.configSection}>
      <div className={classes.configSidebar}>
        {users.map((user, i) => (
          <div key={i}>{ConfigAccordion(user, i, expanded, setExpanded)}</div>
        ))}
      </div>
      <div className={classes.accordionWrapper}>
        <ConfigurationRoutes />
      </div>
    </div>
  );
}
