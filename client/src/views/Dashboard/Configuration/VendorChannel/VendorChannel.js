import React, { useEffect, useState } from "react";
import { ThemeProvider, makeStyles, createMuiTheme } from "@material-ui/core/styles";
import { purple } from "@material-ui/core/colors";
import Avatar from "@material-ui/core/Avatar";
import Switch from "@material-ui/core/Switch";
import Alert from "@material-ui/lab/Alert";
import Grid from "@material-ui/core/Grid";
import VendorChannelDailog from "./VendorChannelDailog";

import { axiosGetData, axiosPatchData } from "utils/util";
import styles from "./VendorChannelStyles.js";
const useStyles = makeStyles(styles);

const theme = createMuiTheme({
  palette: {
    primary: purple
  }
});

const Vendor = () => {
  const classes = useStyles();
  const [vendorsData, setVendorsData] = useState(null);
  const [message, setMessage] = useState(null);

  const loadData = async () => {
    try {
      const vendors = await axiosGetData("/api/vendortenantchannels");
      if (vendors) setVendorsData(vendors.data);
    } catch (err) {
      console.log(err);
      setMessage("Error in displaying data");
    }
  };

  useEffect(() => {
    loadData();
    return () => {};
  }, []);

  const handleStatusChange = async (event, item) => {
    console.log(`logging event value from VTChannel: ${event.target.checked}`);
    console.log(`logging item from VTChannel: ${JSON.stringify(item)}`);
    item.status = event.target.checked ? "Active" : "InActive";
    let body = {
      aliasName: item.aliasName,
      email: item.email,
      password: item.password,
      // TODO: STATUS IS NOT ALLOWED IN UPDATE REQUEST. CHECK WITH SUDHAAKR
      status: item.status,
      isPrimary: item.isPrimary
    };
    const loadVendors = await axiosPatchData("/api/vendortenantchannels/" + item.id, body);
    loadVendors && loadData();
  };

  const onPrimaryChange = async (event, item) => {
    const channelId = item.id;
    delete item.channelId;
    item.isPrimary = event.target.checked ? "1" : "0";
    let body = {
      aliasName: item.aliasName,
      email: item.email,
      password: item.password,
      isPrimary: item.isPrimary
    };
    const loadVendors = await axiosPatchData("/api/vendortenantchannels/" + channelId, body);
    loadVendors && loadData();
  };

  const updateData = () => loadData();

  return (
    <ThemeProvider theme={theme}>
      <h1>
        Vendor Channel Management <VendorChannelDailog updateData={updateData} />
      </h1>
      {message && <Alert severity="error">{message}</Alert>}
      <div className={classes.vendorsWrap}>
        {vendorsData &&
          vendorsData.map((item) => {
            return (
              <Grid
                container
                justify="center"
                direction="column"
                alignItems="center"
                spacing={4}
                className={classes.root}
              >
                <Grid item>
                  <Avatar className={classes.largeAvatar}>{item.aliasName.charAt(0)}</Avatar>
                </Grid>
                <Grid item className={classes.gridSpacing}>
                  <strong>{item.aliasName}</strong>
                </Grid>
                <Grid item className={classes.gridSpacing}>
                  {item.email}
                </Grid>
                <Grid container direction="row" className={classes.lastSection}>
                  {sessionStorage.getItem("vtc") === "RWX" ? (
                    <Grid item xs>
                      <div className={classes.paperWrap}>
                        Status <br />
                        <Switch
                          checked={item.status === "Active" ? true : false}
                          onChange={(event) => handleStatusChange(event, item)}
                          color="primary"
                          inputProps={{ "aria-label": "primary checkbox" }}
                        />
                      </div>
                    </Grid>
                  ) : null}

                  {sessionStorage.getItem("vtc") === "RWX" ? (
                    <Grid item xs>
                      <div className={classes.paperWrap}>
                        IsPrimary <br />
                        <Switch
                          checked={item.isPrimary === "1" ? true : false}
                          onChange={(event) => onPrimaryChange(event, item)}
                          color="primary"
                          inputProps={{ "aria-label": "primary checkbox" }}
                        />
                      </div>
                    </Grid>
                  ) : null}

                  {sessionStorage.getItem("vtc") === "RWX" ? (
                    <Grid item xs>
                      <div className={classes.paperWrap}>
                        <VendorChannelDailog icon="edit" item={item} updateData={updateData} />
                      </div>
                    </Grid>
                  ) : null}
                </Grid>
              </Grid>
            );
          })}
      </div>
    </ThemeProvider>
  );
};

export default Vendor;
