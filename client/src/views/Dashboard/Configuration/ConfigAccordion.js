import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import Typography from "@material-ui/core/Typography";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import styles from "./configAccordionStyles.js";
import { Link } from "react-router-dom";

const useStyles = makeStyles(styles);
export default function ConfigAccordion(user, id, expanded, setExpanded) {
  const classes = useStyles();
  const handleChange = (panel) => (event, newExpanded) => {
    setExpanded(newExpanded ? panel : false);
  };
  const { heading, panelbool, linkTo, icon } = user;

  return (
    <div className={classes.root}>
      {panelbool ? (
        <ExpansionPanel
          expanded={expanded === `panel_${id}`}
          onChange={handleChange(`panel_${id}`)}
          className={classes.expansionPanel}
        >
          <ExpansionPanelSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls={`panel_${id}d-content`}
            id={`panel_${id}d-header`}
            className={(classes.expansionSummary, classes.ExpansionPanelSummaryExpandedIcon)}
          >
            <i className={classes.socialIcons + ` fa fa-${icon}`} />
            <Typography className={classes.heading}>{`${heading}`}</Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails className={classes.expansionDetails}>
            <Typography>
              {user.subHeadings.map((subHeading, i) => (
                <React.Fragment key={subHeading.name}>
                  <Link to={subHeading.linkTo} className={classes.accordionLink}>
                    {sessionStorage.getItem("userRole") !== "iwhyAdmin" ? (
                      subHeading.name === "Vendor" ? (
                        ""
                      ) : (
                        <span className={classes.subHeading}>{subHeading.name}</span>
                      )
                    ) : (
                      <span className={classes.subHeading}>{subHeading.name}</span>
                    )}
                  </Link>
                </React.Fragment>
              ))}
            </Typography>
          </ExpansionPanelDetails>
        </ExpansionPanel>
      ) : (
        <ExpansionPanel expanded={expanded === false} className={classes.expansionPanel}>
          <ExpansionPanelSummary>
            <Typography className={classes.heading}>
              <Link to={`${linkTo}`} className={classes.link}>{`${heading}`}</Link>
            </Typography>
          </ExpansionPanelSummary>
        </ExpansionPanel>
      )}
    </div>
  );
}
