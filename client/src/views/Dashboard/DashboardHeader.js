import React from "react";
import styles from "./dashboardStyles.js";
import { useHistory } from "react-router-dom";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import { makeStyles } from "@material-ui/core/styles";
import AccountCircle from "@material-ui/icons/AccountCircle";
import image from "assets/img/iwhylogo.jpg";
import MenuItem from "@material-ui/core/MenuItem";
import Menu from "@material-ui/core/Menu";
import SettingsIcon from "@material-ui/icons/Settings";
import MoreIcon from "@material-ui/icons/MoreVert";
import { NavLink } from "react-router-dom";
import { clearSession } from "../../utils/util";
import DashboardConfig from "./dashboardConfig";

const useStyles = makeStyles(styles);

export default function DashboardHeader() {
  const classes = useStyles();
  const history = useHistory();
  const routes = DashboardConfig.dashboardRoutes;
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [mobileMoreAnchorEl, setMobileMoreAnchorEl] = React.useState(null);
  const menuId = "primary-search-account-menu";

  const isMenuOpen = Boolean(anchorEl);
  const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);

  const handleProfileMenuOpen = (event) => setAnchorEl(event.currentTarget);

  const handleMobileMenuClose = () => setMobileMoreAnchorEl(null);

  const handleMenuClose = () => {
    setAnchorEl(null);
    handleMobileMenuClose();
  };

  const logout = () => clearSession(history);

  const handleMobileMenuOpen = (event) => setMobileMoreAnchorEl(event.currentTarget);

  const renderMenu = (
    <Menu anchorEl={anchorEl} id={menuId} keepMounted open={isMenuOpen} onClose={handleMenuClose}>
      <p className={classes.userInfo}>
        Welcome <br />
        <span>{sessionStorage.user}</span>
      </p>

      <MenuItem onClick={handleMenuClose}>Profile</MenuItem>
      <MenuItem onClick={handleMenuClose}>My account</MenuItem>
      <MenuItem onClick={logout}>Log out</MenuItem>
    </Menu>
  );

  const mobileMenuId = "primary-search-account-menu-mobile";
  const renderMobileMenu = (
    <Menu
      className={classes.threeDotsMenu}
      anchorEl={mobileMoreAnchorEl}
      id={mobileMenuId}
      keepMounted
      open={isMobileMenuOpen}
      onClose={handleMobileMenuClose}
    >
      {routes.map((item, index) => (
        <MenuItem key={`dashboard-routes-${index}`}>
          <NavLink to={item.routePath}>{item.route}</NavLink>
        </MenuItem>
      ))}
    </Menu>
  );
  return (
    <div className={classes.root}>
      <AppBar position="static" className={classes.appBar}>
        <Toolbar>
          <NavLink exact to="/dashboard">
            <img src={image} height="30" alt="Logo" />
          </NavLink>
          <div className={`${classes.headerLinks}`}>
            <nav className={`${classes.sectionDesktop}`}>
              <ul>
                {routes.map((item, index) => (
                  <li key={`dashboard-routes-mobile-${index}`}>
                    <NavLink
                      exact
                      activeClassName={classes.active}
                      to={item.routePath}
                      className={classes.navLink}
                    >
                      {item.route}
                    </NavLink>
                  </li>
                ))}
              </ul>
            </nav>
          </div>
          <div className={classes.sectionMobile}>
            <IconButton
              aria-label="show more"
              aria-controls={mobileMenuId}
              aria-haspopup="true"
              onClick={handleMobileMenuOpen}
              color="inherit"
            >
              <MoreIcon />
            </IconButton>
          </div>
          <NavLink to="/dashboard/config" className={classes.settingsIcon}>
            <SettingsIcon />
          </NavLink>
          <IconButton
            edge="end"
            aria-label="account of current user"
            aria-controls={menuId}
            aria-haspopup="true"
            onClick={handleProfileMenuOpen}
            color="inherit"
          >
            <AccountCircle />
          </IconButton>
        </Toolbar>
      </AppBar>
      {renderMobileMenu}
      {renderMenu}
    </div>
  );
}
