export default {
  dashboardRoutes: [
    {
      route: "Messages",
      routePath: "/dashboard/mail"
    },
    {
      route: "Orders",
      routePath: "/dashboard/orders"
    },
    {
      route: "Products",
      routePath: "/dashboard/products"
    }
  ]
};
