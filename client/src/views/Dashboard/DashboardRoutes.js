import React from "react";
import { Switch } from "react-router-dom";
import ProtectedRoute from "common/approuter/ProtectedRoute";
import DashboardLanding from "./DashboardLanding";
import Messages from "./Messages/Messages";
import Orders from "./Orders/Orders.js";
import Products from "./Products/Products.js";
import Configuration from "./Configuration/Configuration";

const DashboardRoutes = () => (
  <Switch>
    <ProtectedRoute exact path="/dashboard" component={DashboardLanding} />
    <ProtectedRoute path="/dashboard/mail" component={Messages} />
    <ProtectedRoute path="/dashboard/config" component={Configuration} />
    <ProtectedRoute path="/dashboard/orders" component={Orders} />
    <ProtectedRoute path="/dashboard/products" component={Products} />
  </Switch>
);

export default DashboardRoutes;
