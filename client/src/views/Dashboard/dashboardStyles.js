const dashboardStyles = (theme) => ({
  root: {
    flexGrow: 1
  },
  menuButton: {
    marginRight: theme.spacing(2)
  },
  headerLinks: {
    flexGrow: 1,
    display: "flex",
    alignItems: "center",
    "& nav": {
      width: "100%"
    },
    "& ul": {
      display: "flex",
      flexGrow: 1,
      justifyContent: "center"
    },
    "& ul a": {
      padding: "5px 20px",
      display: "block",
      textDecoration: "none",
      color: "#8e24aa",
      fontWeight: "400"
    },
    "& ul a.active": {
      borderBottom: "2px solid "
    }
  },
  appBar: {
    backgroundColor: "#fff",
    boxShadow: "0 4px 18px 0px rgba(0, 0, 0, 0.12), 0 7px 10px -5px rgba(0, 0, 0, 0.15)",
    color: "#555"
  },
  sectionDesktop: {
    display: "none",
    [theme.breakpoints.up("sm")]: {
      display: "flex"
    }
  },
  sectionMobile: {
    display: "flex",
    [theme.breakpoints.up("sm")]: {
      display: "none"
    }
  },
  settingsIcon: {
    color: "#555",
    display: "inline-flex",
    verticalAlign: "bottom"
  },
  dashboardSection: {
    height: "calc(100vh - 64px)",
    display: "flex"
  },
  threeDotsMenu: {
    "& a": {
      display: "block",
      textDecoration: "none"
    }
  },
  userInfo: {
    padding: "10px",
    margin: 0,
    "& span": {
      color: "#8e24aa"
    }
  }
});

export default dashboardStyles;
