import React from "react";
import Mail from "./Mail";
import { ThemeProvider } from "@material-ui/styles";
import theme from "./theme";

const Messages = () => (
  <ThemeProvider theme={theme}>
    <Mail />
  </ThemeProvider>
);

export default Messages;
