import { axiosPostData } from "../../../../utils/util";

import { MESSAGE_LIST_REQUEST, MESSAGE_LIST_SUCCESS, MESSAGE_LIST_FAIL } from "./mailConstants";

export const listMessages = (body) => async (dispatch) => {
  try {
    dispatch({ type: MESSAGE_LIST_REQUEST });

    const { data } = await axiosPostData(`/api/channel/messages/getMessages`, body);

    dispatch({
      type: MESSAGE_LIST_SUCCESS,
      payload: data
    });
  } catch (error) {
    dispatch({
      type: MESSAGE_LIST_FAIL,
      payload:
        error.response && error.response.data.message ? error.response.data.message : error.message
    });
  }
};
