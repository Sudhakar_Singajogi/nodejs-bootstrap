import React, { useState, useEffect } from "react";
import clsx from "clsx";
import { makeStyles } from "@material-ui/styles";
import { useDispatch, useSelector } from "react-redux";
// import axios from "utils/axios";
import Page from "../../../../components/Page/Page";
import { listMessages } from "./mailActions";

import { EmailFolders, EmailList, EmailDetails } from "./components";

const useStyles = makeStyles((theme) => ({
  root: {
    height: "100%",
    width: "100%",

    display: "flex",
    overflow: "hidden",
    transition: theme.transitions.create("transform", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen
    }),
    "@media (max-width: 780px)": {
      "& $emailFolders, & $emailList, & $emailDetails": {
        flexBasis: "100%",
        width: "100%",
        maxWidth: "none",
        flexShrink: "0",
        transition: "transform .5s ease",
        transform: "translateX(0)"
      }
    }
  },
  openFolder: {
    "@media (max-width: 780px)": {
      "& $emailFolders, & $emailList, & $emailDetails": {
        transform: "translateX(-100%)"
      }
    }
  },
  emailFolders: {
    flexBasis: 280,
    flexShrink: 0,
    flexGrow: 0,
    borderRight: `1px solid ${theme.palette.divider}`
  },
  emailList: {
    flexGrow: 1
  },
  emailDetails: {
    flexGrow: 1
  }
}));

const Mail = () => {
  const dispatch = useDispatch();
  const classes = useStyles();
  const [emails, setEmails] = useState([]);

  const [openFolder, setOpenFolder] = useState(false);
  const [selectedEmail, setSelectedEmail] = useState(null);

  const messagesList = useSelector((state) => state.messagesList);
  const { mails } = messagesList;

  useEffect(() => {
    // console.log(`logging mails length from Mails comp: ${JSON.stringify(mails.length)}`);
    // console.log(`logging emails length Mails comp: ${JSON.stringify(emails.length)}`);

    // let body = {
    //   channelId: "72ea4091-c86a-47e0-a8f6-c75e3c056582",
    //   folderName: "Inbox"
    // };

    let body = {
      channelId: "b87d2d16-cf1b-4367-ae80-fcca29c53e55",
      folderName: "Inbox"
    };
    if (mails.length === 0) {
      dispatch(listMessages(body));
    } else {
      setEmails(mails);
    }
  }, [dispatch, emails, mails.length]);

  const handleFolderOpen = () => {
    setOpenFolder(true);
    setSelectedEmail(null);
  };

  const handleFolderClose = () => {
    setOpenFolder(false);
  };

  const handleEmailOpen = (event) => {
    // console.log(`logging selected emails from Mails comp: ${JSON.stringify(emails[0])}`);

    setSelectedEmail(emails[0]);
  };

  const handleEmailClose = () => {
    setSelectedEmail(null);
  };

  return (
    <Page
      className={clsx({
        [classes.root]: true,
        [classes.openFolder]: openFolder
      })}
      title="Mail"
    >
      <EmailFolders className={classes.emailFolders} onFolderOpen={handleFolderOpen} />
      {selectedEmail ? (
        <EmailDetails
          className={classes.emailDetails}
          email={selectedEmail}
          onEmailClose={handleEmailClose}
        />
      ) : (
        <EmailList
          className={classes.emailList}
          emails={mails}
          onBack={handleFolderClose}
          onEmailOpen={handleEmailOpen}
        />
      )}
    </Page>
  );
};

export default Mail;
