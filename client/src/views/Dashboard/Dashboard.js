import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import styles from "./dashboardStyles.js";
import DashboardRoutes from "./DashboardRoutes.js";
import DashboardHeader from "./DashboardHeader.js";

const useStyles = makeStyles(styles);

export default function Dashboard() {
  const classes = useStyles();
  return (
    <>
      <DashboardHeader />
      <section className={classes.dashboardSection}>
        <DashboardRoutes />
      </section>
    </>
  );
}
