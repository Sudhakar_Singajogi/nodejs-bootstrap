import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import Header from "../../../components/Header/Header.js";
import Button from "../../../components/CustomButtons/Button.js";
import image from "assets/img/bg10.jpg";
import styles from "./styles/navbarsStyle.js";

const useStyles = makeStyles(styles);

export default function SectionNavbars() {
  const classes = useStyles();

  return (
    <div
      className={classes.section}
      style={{ backgroundImage: "url(" + image + ")", height: "310px" }}
    >
      <Header
        brand=""
        fixed
        headerLogo
        rightLinks={
          <List className={classes.list}>
            <ListItem className={classes.listItem}>
              <Button
                href="#pablo"
                className={classes.navLink}
                onClick={(e) => e.preventDefault()}
                color="transparent"
              >
                Portfolio
              </Button>
            </ListItem>
            <ListItem className={classes.listItem}>
              <Button
                href="#pablo"
                className={classes.navLink}
                onClick={(e) => e.preventDefault()}
                color="transparent"
              >
                Services
              </Button>
            </ListItem>
            <ListItem className={classes.listItem}>
              <Button
                href="#pablo"
                className={classes.navLink}
                onClick={(e) => e.preventDefault()}
                color="transparent"
                data-testid="test-contact"
              >
                Contact
              </Button>
            </ListItem>
            <ListItem className={classes.listItem}>
              <Button
                href="#pablo"
                className={classes.registerNavLink}
                onClick={(e) => e.preventDefault()}
                color="rose"
                round
              >
                Speak with us
              </Button>
            </ListItem>
          </List>
        }
      />
    </div>
  );
}
