import React, { useState } from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";

// core components
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import NavPills from "components/NavPills/NavPills.js";

import styles from "./styles/pillsStyle.js";
import Accordion from "../../Dashboard/Configuration/Accordion.js";
import AccordionRoutes from "../../Dashboard/Configuration/ConfigurationRoutes.js";

import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import CONFIG from "utils/config";
let users = CONFIG.users;
const useStyles = makeStyles(styles);

export default function SectionPillsAdmin() {
  const classes = useStyles();
  const [expanded, setExpanded] = useState();
  return (
    <div className={classes.section}>
      <div>
        <div id="navigation-pills">
          <GridContainer>
            <GridItem xs={12} sm={12}>
              <NavPills
                tabs={[
                  {
                    tabButton: "Messages",

                    tabContent: (
                      <div style={{ background: "#fcf" }}>
                        <p>
                          Collaboratively administrate empowered markets via plug-and-play networks.
                          Dynamically procrastinate B2C users after installed base benefits.
                        </p>
                        <br />
                        <p>
                          Dramatically visualize customer directed convergence without revolutionary
                          ROI. Collaboratively administrate empowered markets via plug-and-play
                          networks. Dynamically procrastinate B2C users after installed base
                          benefits.
                        </p>
                        <br />
                        <p>
                          Dramatically visualize customer directed convergence without revolutionary
                          ROI. Collaboratively administrate empowered markets via plug-and-play
                          networks. Dynamically procrastinate B2C users after installed base
                          benefits.
                        </p>
                      </div>
                    )
                  },
                  {
                    tabButton: "Orders",

                    tabContent: (
                      <span>
                        <p>
                          Efficiently unleash cross-media information without cross-media value.
                          Quickly maximize timely deliverables for real-time schemas.
                        </p>
                        <br />
                        <p>
                          Dramatically maintain clicks-and-mortar solutions without functional
                          solutions. Dramatically visualize customer directed convergence without
                          revolutionary ROI. Collaboratively administrate empowered markets via
                          plug-and-play networks. Dynamically procrastinate B2C users after
                          installed base benefits.
                        </p>
                      </span>
                    )
                  },
                  {
                    tabButton: "Products",

                    tabContent: (
                      <span>
                        <p>
                          Collaboratively administrate empowered markets via plug-and-play networks.
                          Dynamically procrastinate B2C users after installed base benefits.
                        </p>
                        <br />
                        <p>
                          Dramatically visualize customer directed convergence without revolutionary
                          ROI. Collaboratively administrate empowered markets via plug-and-play
                          networks. Dynamically procrastinate B2C users after installed base
                          benefits.
                        </p>
                        <br />
                        <p>
                          Dramatically visualize customer directed convergence without revolutionary
                          ROI. Collaboratively administrate empowered markets via plug-and-play
                          networks. Dynamically procrastinate B2C users after installed base
                          benefits.
                        </p>
                      </span>
                    )
                  },
                  {
                    tabButton: "Configuration",

                    tabContent: (
                      <div style={{ background: "#f2f4f7" }}>
                        <Router>
                          <div style={{ background: "#f2f4f7" }}>
                            <div style={{ width: "300px", float: "left" }}>
                              <div>
                                {users.map((user, i) => Accordion(user, i, expanded, setExpanded))}
                              </div>
                            </div>
                            <div style={{ float: "left" }}>
                              <AccordionRoutes />
                            </div>
                          </div>
                        </Router>
                      </div>
                    )
                  }
                ]}
              />
            </GridItem>
          </GridContainer>
        </div>
      </div>
    </div>
  );
}
