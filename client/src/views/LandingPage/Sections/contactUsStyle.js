const contactUsStyle = {
  mapContainer: {
    height: "400px",
    "@media (min-width: 768px)": {
      height: `650px`
    }
  },
  contactInfoWrapper: {
    position: "relative",
    marginTop: "40px"
  },
  container: {
    zIndex: "2",
    position: "relative",
    color: "#FFF",
    margin: "0 auto",
    maxWidth: "500px",
    "@media (min-width: 575px)": {
      marginTop: "-100px"
    },
    "@media (min-width: 768px)": {
      position: "absolute",
      top: "10%",
      left: "5%",
      marginTop: "0"
    }
  },
  cardHeaderContactUs: {
    color: "#FFF",
    boxShadow: "0 4px 20px 0px rgba(0, 0, 0, 0.14), 0 7px 10px -5px rgba(233, 30, 99, 0.4)",
    padding: "20px",
    margin: "-25px 15px 0",
    "& h4": {
      margin: 0,
      fontSize: "18px",
      textAlign: "center"
    }
  },
  contactBtn: {
    float: "right"
  },
  thanksMessage: {
    color: "#8e24aa",
    fontSize: "18px"
  }
};

export default contactUsStyle;
