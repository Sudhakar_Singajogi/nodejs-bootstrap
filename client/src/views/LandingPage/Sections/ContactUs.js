import React, { useState, Fragment } from "react";
import { useFormik } from "formik";
import { ThemeProvider, makeStyles, createMuiTheme } from "@material-ui/core/styles";
import { purple } from "@material-ui/core/colors";

import axios from "axios";

// @material-ui/icons

// core components
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import Button from "components/CustomButtons/Button";
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardBody from "components/Card/CardBody.js";
import RoomIcon from "@material-ui/icons/Room";
import PermPhoneMsgIcon from "@material-ui/icons/PermPhoneMsg";
import InfoArea from "components/InfoArea/InfoArea.js";
import styles from "./contactUsStyle.js";
import CustomMatInput from "components/CustomMatInput/CustomMatInput";

import { GoogleMaps } from "components/Maps/GoogleMaps";
import AllSchemas from "utils/validationSchema";
import CONFIG from "utils/config";
let google = CONFIG.maps.google;

const theme = createMuiTheme({
  palette: {
    primary: purple
  }
});

const useStyles = makeStyles(styles);

export default function ContactUs() {
  const classes = useStyles();
  const [isSubmitionCompleted, setSubmitionCompleted] = useState(false);
  const validationSchema = AllSchemas.contactSchema;
  const formik = useFormik({
    initialValues: {
      fname: "",
      lname: "",
      email: "",
      message: "",
      contactNumber: ""
    },
    validationSchema,
    onSubmit: (values) => {
      values.name = values.fname + values.lname;
      delete values.fname;
      delete values.lname;
      axios.post("/api/contacts", values).then((resp) => {
        setSubmitionCompleted(true);
      });
    }
  });
  return (
    <div className={classes.contactInfoWrapper} id="contactUs">
      <GoogleMaps
        googleMapURL={`${google.url}`}
        {...google}
        loadingElement={<div style={{ height: `100%` }} />}
        containerElement={<div className={classes.mapContainer} />}
        mapElement={<div style={{ height: `100%` }} />}
      />
      <div className={classes.container}>
        <Card>
          <CardHeader color="primary" className={(classes.cardHeader, classes.cardHeaderContactUs)}>
            <h4> Contact Us </h4>{" "}
          </CardHeader>{" "}
          <CardBody>
            <ThemeProvider theme={theme}>
              <form onSubmit={formik.handleSubmit} noValidate autoComplete="off">
                <GridContainer>
                  <GridItem xs={12} sm={6} md={6}>
                    <InfoArea
                      title="Give us a ring"
                      description="Phone: 07873 232250                   
                        Email: iwhysolution @gmail.com,
                        Info @iwhy.co.uk "
                      icon={PermPhoneMsgIcon}
                    />{" "}
                  </GridItem>{" "}
                  <GridItem xs={12} sm={6} md={6}>
                    <InfoArea
                      title="Find us at the office"
                      description="Iwhy solutions limited, C5, seedbed centre, Davidson way, Romford , London Rm7 0az"
                      icon={RoomIcon}
                    />{" "}
                  </GridItem>{" "}
                  {!isSubmitionCompleted && (
                    <Fragment>
                      <GridItem xs={12} sm={6} md={6}>
                        <CustomMatInput label="First Name*" name="fname" {...formik} />
                      </GridItem>
                      <GridItem xs={12} sm={6} md={6}>
                        <CustomMatInput label="Last Name*" name="lname" {...formik} />
                      </GridItem>{" "}
                      <GridItem xs={12} sm={6} md={6}>
                        <CustomMatInput label="Email*" name="email" {...formik} />
                      </GridItem>{" "}
                      <GridItem xs={12} sm={6} md={6}>
                        <CustomMatInput label="Phone Number*" name="contactNumber" {...formik} />
                      </GridItem>{" "}
                      <GridItem xs={12}>
                        <CustomMatInput
                          label="Message*"
                          name="message"
                          {...formik}
                          multiline
                          rows={4}
                        />{" "}
                      </GridItem>
                      <GridItem xs={12}>
                        <div className={classes.contactBtn}>
                          <Button type="submit" variant="contained" color="primary">
                            SEND MESSAGE{" "}
                          </Button>{" "}
                        </div>{" "}
                      </GridItem>{" "}
                    </Fragment>
                  )}{" "}
                  {isSubmitionCompleted && (
                    <GridItem xs={12} color="primary">
                      <p className={classes.thanksMessage}>
                        Thanks for contacting us.We 'll get in touch with you as soon as possible.{" "}
                      </p>{" "}
                    </GridItem>
                  )}{" "}
                </GridContainer>{" "}
              </form>{" "}
            </ThemeProvider>{" "}
          </CardBody>{" "}
        </Card>{" "}
      </div>{" "}
    </div>
  );
}
