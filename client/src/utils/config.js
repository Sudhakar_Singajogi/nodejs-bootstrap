export default {
  pageTitle: "",
  url: "",
  footerNote: "made for a better web.",
  maps: {
    google: {
      key: "AIzaSyDdy2YhPye1He1vuAq3x5FHIDN-OtUy2tM",
      url:
        "https://maps.googleapis.com/maps/api/js?key=AIzaSyDdy2YhPye1He1vuAq3x5FHIDN-OtUy2tM&v=3.exp&libraries=geometry,drawing,places",
      position: { lat: 51.570265, lng: 0.18424 },
      defaultZoom: 20
    }
  },
  home: {
    ScrollspyNav: {
      active: true,
      scrollTargetIds: ["services", "ourTeam", "contactUs"],
      offset: 10,
      scrollDuration: "1000"
    },
    headerNav: [
      {
        id: "services",
        label: "Services",
        href: "#services"
      },
      {
        id: "ourTeam",
        label: "Our Team",
        href: "#ourTeam"
      },
      {
        id: "contactUs",
        label: "Contact us",
        href: "#contactUs"
      },
      {
        id: "signIn",
        label: "Login",
        modal: "signIn"
      },
      {
        id: "fa-twitter",
        label: "twitter",
        icon: "fa-twitter",
        href: "https://twitter.com"
      },
      {
        id: "fa-facebook",
        label: "facebook",
        icon: "fa-facebook",
        href: "https://facebook.com"
      },
      {
        id: "fa-instagram",
        label: "instagram",
        icon: "fa-instagram",
        href: "https://instagram.com"
      }
    ]
  }
};
