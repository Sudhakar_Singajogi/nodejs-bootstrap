import axios from "axios";

const LocalStorageService = (function () {
  var _service;
  function _getService() {
    if (!_service) {
      _service = this;
      return _service;
    }
    return _service;
  }
  function _setToken(tokenObj) {
    localStorage.setItem("accessToken", tokenObj.access_token);
    localStorage.setItem("refresh_token", tokenObj.refresh_token);
  }
  function _getAccessToken() {
    return localStorage.getItem("accessToken");
  }
  function _getRefreshToken() {
    return localStorage.getItem("refresh_token");
  }
  function _clearToken() {
    localStorage.removeItem("access_token");
    localStorage.removeItem("refresh_token");
  }
  return {
    getService: _getService,
    setToken: _setToken,
    getAccessToken: _getAccessToken,
    getRefreshToken: _getRefreshToken,
    clearToken: _clearToken
  };
})();

const axiosLoginData = async (postData) => {
  return new Promise((resolve, reject) => {
    axios
      .post(`/api/users/login`, postData, {
        headers: {
          "Content-Type": "application/json"
        }
      })
      .then((response) => {
        resolve(response.data);
      })
      .catch((error) => {
        reject(JSON.parse(error.request.response));
      });
  });
};
const axiosGetData = async (url) => {
  let headers = {
    headers: {
      Authorization: "Bearer " + sessionStorage.getItem("accessToken"),
      refreshToken: sessionStorage.getItem("refreshToken"),
      "Content-Type": "application/json"
    }
  };
  return new Promise(async (resolve, reject) => {
    axios
      .get(url, headers)
      .then((response) => {
        resolve(response.data);
      })
      .catch((error) => {
        reject(error ? error : { error: "Invalid data" });
      });
  });
};

const axiosPostData = async (url, postData) => {
  let headers = {
    headers: {
      Authorization: "Bearer " + sessionStorage.getItem("accessToken"),
      refreshToken: sessionStorage.getItem("refreshToken"),
      "Content-Type": "application/json"
    }
  };
  return new Promise(async (resolve, reject) => {
    axios
      .post(url, postData, headers)
      .then((response) => {
        resolve(response.data);
      })
      .catch((error) => {
        reject(error ? error : { error: "Invalid data" });
      });
  });
};

const axiosPatchData = async (url, postData) => {
  let headers = {
    headers: {
      Authorization: "Bearer " + sessionStorage.getItem("accessToken"),
      refreshToken: sessionStorage.getItem("refreshToken"),
      "Content-Type": "application/json"
    }
  };
  return new Promise(async (resolve, reject) => {
    axios
      .patch(url, postData, headers)
      .then((response) => {
        resolve(response.data);
      })
      .catch((error) => {
        // console.log(error);
        reject(error ? error : { error: "Invalid data" });
      });
  });
};

const clearSession = (history) => {
  ["refreshToken", "accessToken", "user"].map((item) => sessionStorage.removeItem(item));
  history.push(`/`);
};

export {
  axiosLoginData,
  axiosPostData,
  axiosGetData,
  clearSession,
  LocalStorageService,
  axiosPatchData
};
