import * as Yup from "yup";
const phoneRegExp = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/;
const ValidationSchema = {
  fname: Yup.string().required("First Name is Required"),
  lname: Yup.string().required("Last Name is Required"),
  password: Yup.string().required("Password is Required"),
  changepassword: Yup.string()
    .required("Required")
    .when("password", {
      is: (val) => (val && val.length > 0 ? true : false),
      then: Yup.string().oneOf([Yup.ref("password")], "Both password need to be the same")
    }),
  email: Yup.string().email().required("Email is Required"),
  contactNumber: Yup.string()
    .required("Required")
    .matches(phoneRegExp, "Phone number is not valid"),
  message: Yup.string().required("Required"),
  companyName: Yup.string().required().min(3),
  primaryContactName: Yup.string().required().min(5),
  primaryContactNumber: Yup.string()
    .required("Required")
    .matches(phoneRegExp, "Phone number is not valid"),
  primaryEmail: Yup.string().email().required()
};

const contactSchema = Yup.object().shape({
  fname: ValidationSchema.fname,
  lname: ValidationSchema.lname,
  email: ValidationSchema.email,
  contactNumber: ValidationSchema.contactNumber,
  message: ValidationSchema.message
});

const loginSchema = Yup.object().shape({
  email: ValidationSchema.email,
  password: ValidationSchema.password
});

const vendorSchema = Yup.object().shape({
  companyName: ValidationSchema.companyName,
  primaryContactName: ValidationSchema.primaryContactName,
  primaryContactNumber: ValidationSchema.primaryContactNumber,
  primaryEmail: ValidationSchema.primaryEmail
});

export default {
  contactSchema,
  loginSchema,
  vendorSchema
};
