import { createStore, combineReducers, applyMiddleware } from "redux";
import thunk from "redux-thunk";
// import logger from "redux-logger";
import { composeWithDevTools } from "redux-devtools-extension";

import { loginReducer } from "../components/LoginDailog/_duck/reducer";
import {
  permissiontListReducer,
  permissionCreateReducer,
  permissionDetailsReducer
} from "../views/Dashboard/Configuration/Permission/_duck/reducer";

import {
  rolesListReducer,
  roleCreateReducer,
  roleDetailsReducer,
  roleUpdateReducer
} from "../views/Dashboard/Configuration/Roles/_duck/reducer";

import {
  usersListReducer,
  userCreateReducer,
  userDetailsReducer,
  userUpdateReducer
} from "../views/Dashboard/Configuration/Users/_duck/reducer";

import { messageListReducer } from "../views/Dashboard/Messages/Mail/mailReducers";

const reducer = combineReducers({
  auth: loginReducer,
  permissionList: permissiontListReducer,
  permissionCreate: permissionCreateReducer,
  permissionDetails: permissionDetailsReducer,
  rolesList: rolesListReducer,
  roleCreate: roleCreateReducer,
  roleDetails: roleDetailsReducer,
  updateRole: roleUpdateReducer,
  usersList: usersListReducer,
  userCreate: userCreateReducer,
  userDetails: userDetailsReducer,
  updateUser: userUpdateReducer,
  messagesList: messageListReducer
});

const initialState = {};

const middleware = [thunk];
//cmnt
const store = createStore(
  reducer,
  initialState,
  composeWithDevTools(applyMiddleware(...middleware))
);

export default store;
