import React, { Component } from "react";
import { Redirect, Route } from "react-router-dom";

const ProtectedRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={(props) =>
      sessionStorage.getItem("accessToken") ? <Component {...props} /> : <Redirect to="/" />
    }
  />
);

// class ProtectedRoute extends Component {
//   render() {
//     console.log(JSON.stringify(this.props));
//     const Component = this.props.component;
//     const isAuthenticated = sessionStorage.getItem("accessToken");

//     return isAuthenticated ? <Component /> : <Redirect to={{ pathname: "/" }} />;
//   }
// }

export default ProtectedRoute;
