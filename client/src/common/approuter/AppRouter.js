import React from "react";
import { Switch, Route } from "react-router-dom";
import ProtectedRoute from "./ProtectedRoute";
import Components from "views/Components/Components.js";
import LandingPage from "views/LandingPage/LandingPage.js";
import ProfilePage from "views/ProfilePage/ProfilePage.js";
import LoginPage from "views/LoginPage/LoginPage.js";
import Dashboard from "views/Dashboard/Dashboard.js";

import { Home } from "views/home";

const AppRouter = () => (
  <Switch>
    <Route exact path="/" component={Home} />
    <Route path="/landing-page" component={LandingPage} />
    <Route path="/profile-page" component={ProfilePage} />
    <Route path="/login-page" component={LoginPage} />
    <Route path="/Components" component={Components} />
    <ProtectedRoute path="/dashboard" component={Dashboard} />
  </Switch>
);

export default AppRouter;
