import React from "react";

import AppRouter from "common/approuter/AppRouter";

const App = () => {
  return <AppRouter />;
};

export default App;
