import React from "react";
import TextField from "@material-ui/core/TextField";
import { makeStyles } from "@material-ui/core/styles";
import styles from "./styles";
const useStyles = makeStyles(styles);

const CustomMatInput = (props) => {
  const classes = useStyles();
  const {
    errors,
    touched,
    handleChange,
    handleBlur,
    label,
    name,
    values,
    multiline,
    rows,
    type,
    errorTxt
  } = props;
  let helperText = errorTxt && errors[name] && touched[name] && errors[name];
  return (
    <TextField
      error={errors[name] && touched[name]}
      label={label}
      name={name}
      className={classes.textField}
      onChange={handleChange}
      onBlur={handleBlur}
      fullWidth
      value={values[name]}
      multiline={multiline}
      rows={rows}
      type={type || "text"}
      helperText={helperText}
    />
  );
};

export default CustomMatInput;
