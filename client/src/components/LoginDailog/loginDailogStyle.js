const loginDailogStyle = {
  cardHeader: {
    textAlign: "center",
    margin: "-40px 20px 0",
    padding: "0"
  },
  cardFooter: {
    justifyContent: "flex-end",
    display: "flex",
    marginTop: "10px",
    "& button": {
      marginLeft: "10px"
    }
  }
};

export default loginDailogStyle;
