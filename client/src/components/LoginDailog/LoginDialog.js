/*eslint-disable*/
import React, { useState, useEffect, Fragment } from "react";
import Alert from "@material-ui/lab/Alert";
import { useHistory } from "react-router-dom";
import { useFormik } from "formik";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { useDispatch, useSelector } from "react-redux";

import Slide from "@material-ui/core/Slide";
// react components for routing our app without refresh
import Card from "components/Card/Card.js";
import Dialog from "@material-ui/core/Dialog";
import CustomMatInput from "components/CustomMatInput/CustomMatInput";

import CardBody from "components/Card/CardBody.js";
import CardHeader from "components/Card/CardHeader.js";
import GridItem from "components/Grid/GridItem.js";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import Button from "components/CustomButtons/Button.js";
import GridContainer from "components/Grid/GridContainer.js";
import * as loginOperations from "./_duck/operation";
import { getUserInfo } from "./_duck/action";
import AllSchemas from "utils/validationSchema";

import styles from "./loginDailogStyle.js";
const useStyles = makeStyles(styles);
const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="down" ref={ref} {...props} />;
});

Transition.displayName = "Transition";
const LoginDialog = ({ label }) => {
  const dispatch = useDispatch();
  const classes = useStyles();
  const [classicModal, setClassicModal] = useState(false);
  const history = useHistory();

  const validationSchema = AllSchemas.loginSchema;

  const formik = useFormik({
    initialValues: {
      email: "",
      password: ""
    },
    validationSchema,
    onSubmit: (values) => {
      dispatch(getUserInfo(values, history));
    }
  });

  const auth = useSelector((state) => state.auth);
  const { userInfo, userError } = auth;

  return (
    <Fragment>
      <Button color="transparent" onClick={() => setClassicModal(true)} className={classes.navLink}>
        {label}
      </Button>

      <Dialog
        open={classicModal}
        TransitionComponent={Transition}
        keepMounted
        aria-labelledby="classic-modal-slide-title"
        aria-describedby="classic-modal-slide-description"
        maxWidth="xs"
        PaperProps={{
          style: {
            overflowY: "inherit"
          }
        }}
      >
        <CardHeader color="primary" className={classes.cardHeader}>
          <h2>Login</h2>
        </CardHeader>
        <CardBody>
          <form onSubmit={formik.handleSubmit} noValidate autoComplete="off">
            {userError && <Alert severity="error">{userError}</Alert>}
            <GridContainer>
              <GridItem xs={12}>
                <CustomMatInput label="Email*" name="email" {...formik} />
              </GridItem>
              <GridItem xs={12}>
                <CustomMatInput label="Password*" name="password" {...formik} />
              </GridItem>
              <GridItem className={classes.cardFooter} xs={12}>
                <Button color="primary" onClick={() => setClassicModal(false)}>
                  Cancel
                </Button>
                <Button type="submit" color="primary">
                  Login
                </Button>
              </GridItem>
            </GridContainer>
          </form>
        </CardBody>
      </Dialog>
    </Fragment>
  );
};

export default LoginDialog;
