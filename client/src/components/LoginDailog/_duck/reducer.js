import { USER_INFO, USER_INFO_ERROR } from "./type";

const INITIAL_STATE = {
  userInfo: {},
  error: ""
};

export const loginReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case USER_INFO:
      return {
        ...state,
        error: "",
        userInfo: action.payload
      };

    case USER_INFO_ERROR:
      return {
        error: action.payload
      };
    default:
      return state;
  }
};
