import axios from "axios";
import { USER_INFO, USER_INFO_ERROR } from "./type";

export const getUserInfo = (body, history) => async (dispatch) => {
  try {
    const { data } = await axios.post("/api/users/login", body);

    sessionStorage.setItem("refreshToken", data.refreshToken);
    sessionStorage.setItem("accessToken", data.accessToken);
    sessionStorage.setItem("user", data.user.email);
    sessionStorage.setItem("userRole", data.user.Role);
    sessionStorage.setItem("messages", data.user.PermisionList[0].Messages);
    sessionStorage.setItem("permissions", data.user.PermisionList[0].Permissions);
    sessionStorage.setItem("roles", data.user.PermisionList[0].Roles);
    sessionStorage.setItem("users", data.user.PermisionList[0].Users);
    sessionStorage.setItem("vtc", data.user.PermisionList[0].VTC);
    sessionStorage.setItem("vtchannel", data.user.PermisionList[0].VTChannel);
    sessionStorage.setItem("vendors", data.user.PermisionList[0].Vendors);

    dispatch({
      type: USER_INFO,
      payload: data.user
    });
    history.push(`/dashboard`);
  } catch (error) {
    dispatch({
      type: USER_INFO_ERROR,
      payload:
        error.response && error.response.data.message ? error.response.data.message : error.message
    });
  }
};
// const getUserInfo = (data) => ({
//   type: Type.USER_INFO,
//   payload: data
// });

// const toggleErrorInfo = (data) => ({
//   type: Type.USER_INFO_ERROR,
//   payload: data
// });

// export { getUserInfo, toggleErrorInfo };
