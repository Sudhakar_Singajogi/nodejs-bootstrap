/*eslint-disable*/
import React from "react";
import { object } from "prop-types";
// react components for routing our app without refresh

// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import Button from "components/CustomButtons/Button.js";
import ListItem from "@material-ui/core/ListItem";
import ScrollspyNav from "react-scrollspy-nav";
import LoginDialog from "../LoginDailog/LoginDialog";

import styles from "./headerLinksStyle.js";

const useStyles = makeStyles(styles);

export default function HeaderLinks({ data }) {
  // console.log(`props.data in HeaderLinks: ${JSON.stringify(data)}`);
  const classes = useStyles();
  return (
    <>
      <List className={classes.list}>
        {data.headerNav &&
          data.headerNav.map((item, index) => (
            <ListItem key={`headerNav-${index}`} className={classes.listItem}>
              {item.modal ? (
                <LoginDialog label={item.label} />
              ) : (
                <Button
                  href={`${item.href}`}
                  color="transparent"
                  target="_target"
                  className={classes.navLink}
                >
                  {item.icon ? (
                    <i className={classes.socialIcons + " fab " + item.icon} />
                  ) : (
                    <span>{item.label}</span>
                  )}
                </Button>
              )}
            </ListItem>
          ))}
      </List>
    </>
  );
}

HeaderLinks.propTypes = {
  data: object
};
