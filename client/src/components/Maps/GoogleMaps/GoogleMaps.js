import React from "react";
import { withScriptjs, withGoogleMap, GoogleMap, Marker } from "react-google-maps";

const GoogleMaps = withScriptjs(
  withGoogleMap((props) => (
    <GoogleMap defaultZoom={props.defaultZoom} defaultCenter={props.position}>
      <Marker position={props.position} />
    </GoogleMap>
  ))
);

export default GoogleMaps;
