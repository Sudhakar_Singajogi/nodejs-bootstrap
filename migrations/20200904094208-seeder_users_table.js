'use strict';

module.exports = {
    up: async(queryInterface, Sequelize) => {
        return Promise.all([queryInterface.bulkInsert('users', [{
                userName: "Sirisha",
                password: "e10adc3949ba59abbe56e057f20f883e",
                email: "sirisha@mailinator.com",
                RoleId: "2",
                status: "Active",
                vendorId: 1,
                createdAt: new Date(),
                updatedAt: new Date(),
            },
            {
                userName: "SaiRam",
                password: "e10adc3949ba59abbe56e057f20f883e",
                email: "sairam@mailinator.com",
                RoleId: "1",
                status: "Active",
                vendorId: 1,
                createdAt: new Date(),
                updatedAt: new Date(),
            }
        ])])
    },

    down: async(queryInterface, Sequelize) => {
        /**
         * Add reverting commands here.
         *
         * Example:
         * await queryInterface.dropTable('users');
         */
    }
};