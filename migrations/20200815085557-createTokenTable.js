'use strict';

module.exports = {
    up: async(queryInterface, Sequelize) => {
        queryInterface.createTable('tokens', {
            id: {
                type: Sequelize.INTEGER(11),
                primaryKey: true,
                autoIncrement: true
            },
            accessToken: {
                type: Sequelize.TEXT,
                allowNull: false
            },
            refreshToken: {
                type: Sequelize.TEXT,
                allowNull: false
            },
            userId: {
                type: Sequelize.INTEGER(11),
                allowNull: false
            },
            status: {
                type: Sequelize.ENUM('Active', 'InActive'),
                allowNull: false
            },
            createdAt: Sequelize.DATE,
            updatedAt: Sequelize.DATE
        })
    },

    down: async(queryInterface, Sequelize) => {
        queryInterface.dropTable('tokens');
    }
};