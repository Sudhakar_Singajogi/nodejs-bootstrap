'use strict';

module.exports = {
    up: async(queryInterface, Sequelize) => {
        queryInterface.createTable('roles', {
            id: {
                type: Sequelize.INTEGER(11),
                primaryKey: true,
                autoIncrement: true
            },
            name: {
                type: Sequelize.STRING(300),
                allowNull: false
            },
            description: {
                type: Sequelize.STRING(300),
                allowNull: false
            },
            permissionId: {
                type: Sequelize.INTEGER(11),
                allowNull: false
            },
            status: {
                type: Sequelize.ENUM('Active', 'InActive'),
                allowNull: false,
                defaultValue: "Active"
            },
            createdAt: Sequelize.DATE,
            updatedAt: Sequelize.DATE
        })
    },

    down: async(queryInterface, Sequelize) => {
        queryInterface.dropTable('roles')
    }
};