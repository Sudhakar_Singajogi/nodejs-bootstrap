'use strict';

module.exports = {
    up: async(queryInterface, Sequelize) => {
        queryInterface.createTable('VTCredentials', {
            id: {
                type: Sequelize.INTEGER(11),
                primaryKey: true,
                autoIncrement: true
            },
            tenantUID: {
                type: Sequelize.STRING(300),
                allowNull: false
            },
            clientId: {
                type: Sequelize.STRING(300),
                allowNull: false
            },
            clientSecret: {
                type: Sequelize.STRING(300),
                allowNull: false
            },
            vendorId: {
                type: Sequelize.INTEGER(11),
                allowNull: false
            },
            status: {
                type: Sequelize.ENUM('Active', 'InActive'),
                allowNull: false
            },
            createdAt: Sequelize.DATE,
            updatedAt: Sequelize.DATE
        })
    },

    down: async(queryInterface, Sequelize) => {
        queryInterface.dropTable('VTCredentials');
    }
};