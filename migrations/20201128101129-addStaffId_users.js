'use strict';

module.exports = {
    up: async(queryInterface, Sequelize) => {

        return Promise.all([
            queryInterface.addColumn(
                'Users', // table name
                'staffId', // new field name
                {
                    type: Sequelize.STRING,
                    allowNull: true,
                    after: "status"
                },
            ),

        ]);

    },

    down: async(queryInterface, Sequelize) => {
        return Promise.all([
            queryInterface.removeColumn('Users', 'staffId')
        ]);
    }
};