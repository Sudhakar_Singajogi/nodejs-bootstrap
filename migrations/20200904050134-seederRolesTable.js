'use strict';

module.exports = {
    up: async(queryInterface, Sequelize) => {
        return Promise.all([queryInterface.bulkInsert('roles', [{
                name: "iwhyAdmin",
                description: "Iwhy Super Adminstartor",
                permissionId: 1,
                status: "Active",
                createdAt: new Date(),
                updatedAt: new Date(),
            },
            {
                name: "SuperAdmin",
                description: "Super Adminstartor",
                permissionId: 2,
                status: "Active",
                createdAt: new Date(),
                updatedAt: new Date(),
            },
            {
                name: "Saff",
                description: "Staff",
                permissionId: 3,
                status: "Active",
                createdAt: new Date(),
                updatedAt: new Date(),

            }
        ])])
    },

    down: async(queryInterface, Sequelize) => {
        /**
         * Add reverting commands here.
         *
         * Example:
         * await queryInterface.dropTable('users');
         */
    }
};