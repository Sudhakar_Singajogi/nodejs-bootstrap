'use strict';

module.exports = {
    up: async(queryInterface, Sequelize) => {
        return Promise.all([queryInterface.bulkInsert('permissions', [{
                name: "RWXD",
                permissionList: '[{\"Permissions\":\"RWX\",\"Roles\":\"RWX\",\"Users\":\"RWX\",\"Vendors\":\"RWX\", \"VTC\":\"RW\", \"VTChannel\":\"RW\", \"Messages\":\"RWX\"}]',
                status: "Active",
                createdAt: new Date(),
                updatedAt: new Date(),
            }, {
                name: "RWX",
                permissionList: '[{\"Permissions\":\"RW\",\"Roles\":\"RWX\",\"Users\":\"RWX\", \"VTC\":\"RW\", \"VTChannel\":\"RW\", \"Messages\":\"RWX\"}]',
                status: "Active",
                createdAt: new Date(),
                updatedAt: new Date(),
            },
            {
                name: "Read",
                permissionList: '[{\"Roles\":\"R\",\"Users\":\"R\",\"Vendors\":\"R\"}]',
                status: "Active",
                createdAt: new Date(),
                updatedAt: new Date(),
            }
        ])])
    },

    down: async(queryInterface, Sequelize) => {
        /**
         * Add reverting commands here.
         *
         * Example:
         * await queryInterface.dropTable('users');
         */
    }
};