'use strict';

module.exports = {
    up: async(queryInterface, Sequelize) => {
        return Promise.all([
            queryInterface.changeColumn('vtcredentials', 'status', {
                type: Sequelize.ENUM('Active', 'InActive'),
                defaultValue: 'Active'
            }),

            queryInterface.changeColumn('vendors', 'status', {
                type: Sequelize.ENUM('Active', 'InActive'),
                defaultValue: 'Active'
            }),
            queryInterface.changeColumn('permissions', 'status', {
                type: Sequelize.ENUM('Active', 'InActive'),
                defaultValue: 'Active'
            }),
            queryInterface.changeColumn('roles', 'status', {
                type: Sequelize.ENUM('Active', 'InActive'),
                defaultValue: 'Active'
            }),
            queryInterface.changeColumn('users', 'status', {
                type: Sequelize.ENUM('Active', 'InActive'),
                defaultValue: 'Active'
            }),
            queryInterface.changeColumn('users', 'status', {
                type: Sequelize.ENUM('Active', 'InActive'),
                defaultValue: 'Active'
            }),
        ]);
    },

    down: async(queryInterface, Sequelize) => {
        return Promise.all([queryInterface.changeColumn('vtcredentials', 'status')]);
    }
};