'use strict';

module.exports = {
    up: async(queryInterface, Sequelize) => {
        queryInterface.createTable('messageConfigurations', {
            id: {
                type: Sequelize.INTEGER(11),
                primaryKey: true,
                autoIncrement: true
            },
            vendorId: {
                type: Sequelize.INTEGER(11),
                allowNull: false
            },
            accountAlias: {
                type: Sequelize.TEXT,
                allowNull: false
            },
            email: {
                type: Sequelize.TEXT,
                allowNull: false
            },
            password: {
                type: Sequelize.STRING(30),
                allowNull: false
            },
            tenantId: {
                type: Sequelize.STRING(300),
                allowNull: false
            },
            clientId: {
                type: Sequelize.STRING(300),
                allowNull: false
            },
            clientSecret: {
                type: Sequelize.STRING(300),
                allowNull: false
            },
            status: {
                type: Sequelize.ENUM('Active', 'InActive'),
                allowNull: false
            },
            createdAt: Sequelize.DATE,
            updatedAt: Sequelize.DATE
        })
    },

    down: async(queryInterface, Sequelize) => {
        queryInterface.dropTable('messageConfiguration');
    }
};