'use strict';

module.exports = {
    up: async(queryInterface, Sequelize) => {
        queryInterface.addColumn('users', 'vendorId', {
            type: Sequelize.INTEGER,
            allowNull: false
        });
    },

    down: async(queryInterface, Sequelize) => {
        queryInterface.removeColumn('users', 'vendorId');
    }
};