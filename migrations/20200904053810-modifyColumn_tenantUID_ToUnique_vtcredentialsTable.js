'use strict';

module.exports = {
    up: async(queryInterface, Sequelize) => {
        return Promise.all([
            queryInterface.sequelize.query('ALTER TABLE `vtcredentials` ADD UNIQUE `unique_tenantUID_index`(`tenantUID`)')
        ]);
    },

    down: async(queryInterface, Sequelize) => {
        return true;
    }
};