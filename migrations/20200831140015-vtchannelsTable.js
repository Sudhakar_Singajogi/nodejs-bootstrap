"use strict";

module.exports = {
    up: async(queryInterface, Sequelize) => {
        queryInterface.createTable("VTChannels", {
            id: {
                type: Sequelize.INTEGER(11),
                primaryKey: true,
                autoIncrement: true
            },
            channelId: {
                type: Sequelize.STRING(300),
                allowNull: false
            },
            vendorId: {
                type: Sequelize.INTEGER(11),
                allowNull: false
            },
            aliasName: {
                type: Sequelize.STRING(300),
                allowNull: false
            },
            email: {
                type: Sequelize.STRING(300),
                allowNull: false
            },
            password: {
                type: Sequelize.STRING(300),
                allowNull: false
            },
            isPrimary: {
                type: Sequelize.ENUM("1", "0"),
                allowNull: false,
                defaultValue: "0"
            },
            status: {
                type: Sequelize.ENUM("Active", "InActive"),
                allowNull: false,
                defaultValue: "Active"
            },
            createdAt: Sequelize.DATE,
            updatedAt: Sequelize.DATE
        });
    },

    down: async(queryInterface, Sequelize) => {
        queryInterface.dropTable("VTChannels");
    }
};