'use strict';

module.exports = {
    up: async(queryInterface, Sequelize) => {
        return Promise.all([
            queryInterface.sequelize.query('ALTER TABLE `users` ADD UNIQUE `uniqueEmail_index`(`email`)')
        ]);
    },

    down: async(queryInterface, Sequelize) => {
        return true;
    }
};