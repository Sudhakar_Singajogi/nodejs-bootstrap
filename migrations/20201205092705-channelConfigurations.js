'use strict';

module.exports = {
    up: async(queryInterface, Sequelize) => {
        queryInterface.createTable("channelConfigurations", {
            id: {
                type: Sequelize.INTEGER(11),
                primaryKey: true,
                autoIncrement: true
            },
            channelId: {
                type: Sequelize.STRING(256),
                allowNull: false
            },
            vendorId: {
                type: Sequelize.INTEGER(11),
                allowNull: false
            },
            email: {
                type: Sequelize.STRING(256),
                allowNull: false
            },
            password: {
                type: Sequelize.STRING(256),
                allowNull: false
            },
            tenantUID: {
                type: Sequelize.STRING(256),
                allowNull: false
            },
            aliasName: {
                type: Sequelize.STRING(256),
                allowNull: false
            },
            clientId: {
                type: Sequelize.STRING(256),
                allowNull: false
            },
            clientSecret: {
                type: Sequelize.STRING(256),
                allowNull: false
            },
            appId: {
                type: Sequelize.STRING(256),
                allowNull: false
            },
            devId: {
                type: Sequelize.STRING(256),
                allowNull: false
            },
            appSecret: {
                type: Sequelize.STRING(256),
                allowNull: false
            },
            isPrimary: {
                type: Sequelize.ENUM("1", "0"),
                allowNull: false,
                defaultValue: "0"
            },
            status: {
                type: Sequelize.ENUM("Active", "InActive"),
                allowNull: false,
                defaultValue: "Active"
            },
            createdAt: Sequelize.DATE,
            updatedAt: Sequelize.DATE
        })
    },

    down: async(queryInterface, Sequelize) => {
        queryInterface.dropTable("channelConfigurations");
    }
};