'use strict';

module.exports = {
    up: async(queryInterface, Sequelize) => {
        queryInterface.createTable('permissions', {
            id: {
                type: Sequelize.INTEGER(11),
                primaryKey: true,
                autoIncrement: true
            },
            name: {
                type: Sequelize.STRING(300),
                allowNull: false
            },
            permissionList: {
                type: Sequelize.JSON,
                allowNull: false
            },
            status: {
                type: Sequelize.ENUM('Active', 'InActive'),
                allowNull: false,
                defaultValue: "Active"
            },
            createdAt: Sequelize.DATE,
            updatedAt: Sequelize.DATE
        });
    },

    down: async(queryInterface, Sequelize) => {
        queryInterface.dropTable('permissions');
    }
};