'use strict';

module.exports = {
    up: async(queryInterface, Sequelize) => {
        queryInterface.createTable('contacts', {
            id: {
                type: Sequelize.INTEGER(11),
                primaryKey: true,
                autoIncrement: true
            },
            name: {
                type: Sequelize.STRING(300),
                allowNull: false
            },
            email: {
                type: Sequelize.STRING(300),
                allowNull: false
            },
            contactNumber: {
                type: Sequelize.STRING(300),
                allowNull: false
            },
            message: Sequelize.TEXT,
            createdAt: Sequelize.DATE,
            updatedAt: Sequelize.DATE
        })
    },

    down: async(queryInterface, Sequelize) => {
        queryInterface.dropTable('contacts')
    }
};