'use strict';

module.exports = {
    up: async(queryInterface, Sequelize) => {
        return Promise.all([queryInterface.bulkInsert('vendors', [{
            vendorUniqueId: "52e4beebaada3bded072376212wdr",
            companyName: "iWHYSolutions",
            primaryContactName: "Sirisha",
            primaryContactNumber: "0123456789",
            primaryEmail: "sirisha@mailinator.com",
            createdAt: new Date(),
            updatedAt: new Date(),
        }])])
    },

    down: async(queryInterface, Sequelize) => {
        /**
         * Add reverting commands here.
         *
         * Example:
         * await queryInterface.dropTable('users');
         */
    }
};