'use strict';

module.exports = {
    up: async(queryInterface, Sequelize) => {
        return Promise.all([
            queryInterface.sequelize.query('ALTER TABLE `vtchannels` ADD UNIQUE `unique_channelId_index`(`channelId`)')
        ]);
    },

    down: async(queryInterface, Sequelize) => {
        return true;
    }
};