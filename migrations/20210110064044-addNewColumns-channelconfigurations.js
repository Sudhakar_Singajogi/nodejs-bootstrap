'use strict';

module.exports = {
    up: async(queryInterface, Sequelize) => {
        queryInterface.addColumn('channelconfigurations', 'siteURL', {
            type: Sequelize.STRING,
            allowNull: false,
            after: "vendorId"
        });

        queryInterface.addColumn('channelconfigurations', 'sandboxURL', {
            type: Sequelize.STRING,
            allowNull: true,
            after: "siteURL"
        });
    },

    down: async(queryInterface, Sequelize) => {
        queryInterface.removeColumn('channelconfigurations', 'siteURL');
        queryInterface.removeColumn('channelconfigurations', 'sandboxURL');
    }
};