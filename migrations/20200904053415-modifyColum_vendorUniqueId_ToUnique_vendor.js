'use strict';

module.exports = {
    up: async(queryInterface, Sequelize) => {
        return Promise.all([
            queryInterface.sequelize.query('ALTER TABLE `vendors` ADD UNIQUE `uniqueRole_index`(`vendorUniqueId`)')
        ]);
    },

    down: async(queryInterface, Sequelize) => {
        return true;
    }
};