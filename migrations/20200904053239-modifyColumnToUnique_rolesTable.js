'use strict';

module.exports = {
    up: async(queryInterface, Sequelize) => {
        return Promise.all([
            queryInterface.sequelize.query('ALTER TABLE `roles` ADD UNIQUE `uniqueRole_index`(`name`)')
        ]);
    },

    down: async(queryInterface, Sequelize) => {
        return true;
    }
};