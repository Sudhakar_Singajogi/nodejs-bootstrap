'use strict';

module.exports = {
    up: async(queryInterface, Sequelize) => {
        queryInterface.createTable('vendors', {
            id: {
                type: Sequelize.INTEGER(11),
                primaryKey: true,
                autoIncrement: true
            },
            vendorUniqueId: {
                type: Sequelize.STRING(300),
                allowNull: false
            },
            companyName: {
                type: Sequelize.TEXT,
                allowNull: false
            },
            primaryContactName: {
                type: Sequelize.TEXT,
                allowNull: false
            },
            primaryContactNumber: {
                type: Sequelize.STRING(30),
                allowNull: false
            },
            primaryEmail: {
                type: Sequelize.STRING(30),
                allowNull: false,
                unique: true
            },
            status: {
                type: Sequelize.ENUM('Active', 'InActive'),
                allowNull: false,
                defaultValue: "Active"
            },
            createdAt: Sequelize.DATE,
            updatedAt: Sequelize.DATE
        })
    },

    down: async(queryInterface, Sequelize) => {
        queryInterface.dropTable('vendors');
    }
};